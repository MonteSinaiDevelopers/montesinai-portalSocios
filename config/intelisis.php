<?php

return [
		'IS_URL' => env('IS_URL', 'localhost'),
		'IS_PORT' => env('IS_PORT', '8050'),
		'IS_USUARIO' => env('IS_USUARIO', 'ISUSER'),
		'IS_CONTRASENA' => env('IS_CONTRASENA', '24f17ed41803452759cd5d7a83fb1200'),
		'IS_EMPRESA' => env('IS_EMPRESA', 'DEMO'),
		'RequestDaysTTL' => 2,
		'ForceCache' => env('IS_FORCECACHE', false),
];
