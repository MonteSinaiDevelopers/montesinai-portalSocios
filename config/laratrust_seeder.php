<?php

return [
    'role_structure' => [
        'administrator' => [
            'users' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
        'user' => [
            'profile' => 'r,u'
        ],
    	'casos' => [
    		'caso' => 'c,r,u,d',
    		'casoevento' => 'c,r,u,d',
    	],
        'directivo-casos' => [
            'caso' => 'c,r,u,d',
            'casoevento' => 'c,r,u,d',
        ],
        'socios' => [
            'socios' => 'r',
        ],
        'socios-cxc' => [
            'socios' => 'r',
            'socioscxc' => 'r',
        ]
    ],
    'permission_structure' => [
    		/*
        'cru_user' => [
            'profile' => 'c,r,u'
        ],
        */
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
