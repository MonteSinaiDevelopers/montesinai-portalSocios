@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Administrar Usuarios</div>

                <div class="panel-body">
                    <table class="UserAdminTable table table-hover table-striped" id="UserAdminTable">
                    	<thead>
                    		<tr>
	                    		<th>ID</th>
	                    		<th>Nombre</th>
	                    		<th>Usuario</th>
	                    		<th>FechaCreacion</th>
	                    		<th>Acciones</th>
	                    	</tr>
                    	</thead>
                    	<tbody>
                    		<tr>
                    			<td colspan="5">Cargando tabla...</td>
                    		</tr>
                    	</tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
$(document).ready(function(){
	$.getJSON(
		"{{ route('user/get') }}",
		{
				
		},
		function(data){
			$('#UserAdminTable').fillTable(data.users, {
				columns: ['id', 'name', 'username', 'created_at', 'actions:edit,delete'],
				editaction: {
					link: '{{ route('user/edit') }}?userid=%%id%%',
					label: '{{ __('common.edit_action') }}'
					},
				deleteaction: {
					link: '{{ route('user/delete') }}?userid=%%id%%',
					label: '{{ __('common.delete_action') }}'
					},
			});
		},
	);
});
@endsection