@extends('layouts.app')

@section('content')
<div class="container">
	<form method="post" action="{{ route('profile/modify') }}">
	{{ csrf_field() }}
	<ul class="nav nav-tabs" id="UserEditTab" role="tablist">
		<li class="nav-item pull-right">
			<button type="submit" class="btn btn-primary">Editar</button>
		</li>
	</ul>
    <div class="tab-content" id="UserEditTabContent">
		<div class="tab-pane active" id="userdetails" role="tabpanel" aria-labelledby="userdetails-tab">
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">Editar Usuario</div>
	
	                <div class="panel-body">
                		<div class="form-group">
                			<label for="name">Nombre</label>
                			<input type="text" name="name" id="name" class="form-control" placeholder="Nombre de la persona" value="{{ $user->name }}"/>
                		</div>
                		<div class="form-group">
                			<label for="username">Usuario</label>
                			<input type="text" name="username" id="username" class="form-control" placeholder="Nombre de usuario para ingresar al sistema" value="{{ $user->username }}"/>
                		</div>
                		<div class="form-group">
                			<label for="email">E-Mail</label>
                			<input type="text" name="email" id="email" class="form-control" placeholder="Correo de la persona" value="{{ $user->email }}"/>
                		</div>
                		<div class="form-group">
                			<label for="password">Contraseña</label>
                			<input type="password" name="password" id="password" class="form-control" placeholder="Ingresar una contraseña nueva para cambiar. Dejar en blanco para no cambiar" value="" />
                		</div>
                		<div class="form-group">
                			<label for="password_confirmation">Confirmar Contraseña</label>
                			<input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Volver a escribir contraseña en caso de cambiarla" value=""/>
                		</div>
	                </div>
	            </div>
	        </div>
		</div>
    </div>
	</form>
</div>
@endsection