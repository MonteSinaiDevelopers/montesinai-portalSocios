@extends('layouts.app')

@section('content')
<div class="container">
	<form method="post" action="{{ route('user/edit') }}">
	{{ csrf_field() }}
	<ul class="nav nav-tabs" id="UserEditTab" role="tablist">
		<li class="nav-item active">
			<a class="nav-link active" id="userdetails-tab" data-toggle="tab" href="#userdetails" role="tab" aria-controls="userdetails" aria-expanded="true">Detalles</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="userperms-tab" data-toggle="tab" href="#userperms" role="tab" aria-controls="userperms" aria-expanded="true">Permisos</a>
		</li>
		<li class="nav-item pull-right">
			<button type="submit" class="btn btn-primary">Editar</button>
		</li>
	</ul>
    <div class="tab-content" id="UserEditTabContent">
		<div class="tab-pane active" id="userdetails" role="tabpanel" aria-labelledby="userdetails-tab">
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">Editar Usuario</div>
	
	                <div class="panel-body">
                		<input type="hidden" name="userid" id="userdetails-userid" value="{{ $user->id }}" />
                		<div class="form-group">
                			<label for="name">Nombre</label>
                			<input type="text" name="name" id="name" class="form-control" placeholder="Nombre de la persona" value="{{ $user->name }}"/>
                		</div>
                		<div class="form-group">
                			<label for="username">Usuario</label>
                			<input type="text" name="username" id="username" class="form-control" placeholder="Nombre de usuario para ingresar al sistema" value="{{ $user->username }}"/>
                		</div>
                		<div class="form-group">
                			<label for="email">E-Mail</label>
                			<input type="text" name="email" id="email" class="form-control" placeholder="Correo de la persona" value="{{ $user->email }}"/>
                		</div>
                		<div class="form-group">
                			<label for="password">Contraseña</label>
                			<input type="password" name="password" id="password" class="form-control" placeholder="Ingresar una contraseña nueva para cambiar. Dejar en blanco para no cambiar" value="" />
                		</div>
                		<div class="form-group">
                			<label for="password_confirmation">Confirmar Contraseña</label>
                			<input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Volver a escribir contraseña en caso de cambiarla" value=""/>
                		</div>
                		<div class="form-group">
                			<label for="usuario_intelisis">Usuario Intelisis</label>
                			<select id="usuario_intelisis" name="usuario_intelisis" class="form-control" size="8">
                				<option value="">Cargando Lista...</option>
                			</select>
                		</div>
                		<div class="form-group">
                			<label for="password_intelisis">Contraseña Intelisis (encriptada)</label>
                			<input type="text" name="password_intelisis" id="password_intelisis" class="form-control" placeholder="Contraseña encriptada" value="{{ $user->password_intelisis }}" />
                			
                		</div>
	                </div>
	            </div>
	        </div>
		</div>
		<div class="tab-pane" id="userperms" role="tabpanel" aria-labelledby="userperms-tab">
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">Roles y Permisos</div>
                		<input type="hidden" name="userid" id="userperms-userid" value="{{ $user->id }}" />
                		
		                @forelse(App\Role::all() as $role)
		                	<div class="panel-body">
		                		<div class="col-md-11 col-md-offset-1">
		                			<div class="form-check">
		                				<label for="userperm-role[{{$role->name}}]>{{ $role->display_name }}"><input type="checkbox" name="userperm-role[{{$role->name}}]" id="userperm-role[{{$role->name}}]" class="form-check-input" {{$user->hasRole($role->name)?'checked="checked"':''}} />{{$role->display_name }}</label>
		                			</div>
		                		</div>
		                		
		                		
		                		@forelse($role->permissions()->get() as $permission)
		                			<div class="form-check">
		                				<label class="form-check-label" for="userperm[{{$role->name}}][{{$permission->name}}]"><input type="checkbox" name="userperm[{{$role->name}}][{{$permission->name}}]" id="userperm[{{$role->name}}][{{$permission->name}}]" class="form-check-input" {{$user->hasRole($role->name)?'checked="checked"':''}} />{{$permission->display_name}}</label>
		                			</div>
		                		@empty
		                			El Rol no tiene permisos
		                		@endforelse
		                	</div>		
		                @empty
		                	No se encontraron Roles
		                @endforelse
	            </div>
			</div>
		</div>
    </div>
	</form>
</div>
@endsection

@section('js')
$(document).ready(function(){
	$.getJSON(
		"{{ route('user/getIntelisisUsersJSON') }}",
		{
		},
		function(data){
			if(data.success == 0){
				$('#usuario_intelisis').empty();
				$.notify({
					message: 'Ocurrio un error al obtener la lista de usuarios',
					type: 'danger'
				});
				return;
			}

			for (Vobj in data.Usuarios){
				if(data.Usuarios[Vobj]['@Usuario'] == '{{ $user->usuario_intelisis or "" }}'){
					data.Usuarios[Vobj]['selected'] = 'selected';
				}
			}
			$('#usuario_intelisis').empty();
			$('#usuario_intelisis').append(Mustache.render('@{{#Usuarios}}<option @{{#selected}}selected="selected"@{{/selected}} value="@{{{@Usuario}}}">@{{{@Nombre}}}</option>@{{/Usuarios}}'
			, data)
			);
		}
	);
});
@endsection