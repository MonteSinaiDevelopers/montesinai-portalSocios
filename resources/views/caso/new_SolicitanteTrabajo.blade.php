@extends('layouts.app')

@section('header')
@endsection

@section('content')
<div class="container">
	<h1>Solicitud de Nuevo Caso</h1>
	
	<form id="CasoCreateForm" method="POST" action="{{ route('caso/new_submit') }}" >
		{{ csrf_field() }}
		<input type="hidden" name="CasoCreateAcceso" id="CasoCreateAcceso" value="{{Session::get('acceso', '0')}}" />
		<input type="hidden" name="CasoCreatePhase" id="CasoCreatePhase" value="2" />

		<div class="btn-group pull-left"> 
	         <a href="#" class="btn btn-primary" onclick="$('#CasoCreatePhase').val(1);this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Anterior</a>
	    </div>
		<div class="btn-group pull-right"> 
	         <a href="#" class="btn btn-primary" onclick="this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>Siguiente</a>
	    </div>

		<fieldset>
			<div class="row">
				<h2>Solicitante</h2>
			</div>
			<div class="row">
				<div class="alert alert-warning" role="alert">
				  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				  <span class="sr-only">Advertencia</span>
				  Todos los campos son obligatorios.
				</div>
			</div>
			<div class="row">
				<h3>Datos de Trabajo</h3>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteNombreTrabajoHasError'] or ''}}">
				<label for="CasoCreateSolicitanteNombreTrabajo" class="col-form-label col-sm-3">Nombre Lugar de Trabajo</label>
				<div class="col-sm-6">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteNombreTrabajo" name="CasoCreateSolicitanteNombreTrabajo" value="{{ request('CasoCreateSolicitanteNombreTrabajo', $resultado_socio->get('NombreTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitantePuestoHasError'] or ''}}">
				<label for="CasoCreateSolicitantePuesto" class="col-form-label col-sm-1">Puesto</label>
				<div class="col-sm-4 col-sm-offset-2">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitantePuesto" name="CasoCreateSolicitantePuesto" value="{{ request('CasoCreateSolicitantePuesto', $resultado_socio->get('Puesto')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteDireccionTrabajoHasError'] or ''}}">
				<label for="CasoCreateSolicitanteDireccionTrabajo" class="col-form-label col-sm-2">Direccion Trabajo</label>
				<div class="col-sm-9 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteDireccionTrabajo" name="CasoCreateSolicitanteDireccionTrabajo" value="{{ request('CasoCreateSolicitanteDireccionTrabajo', $resultado_socio->get('DireccionTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteColoniaTrabajoHasError'] or ''}}">
				<label for="CasoCreateSolicitanteColoniaTrabajo" class="col-form-label col-sm-2">Colonia Trabajo</label>
				<div class="col-sm-6 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteColoniaTrabajo" name="CasoCreateSolicitanteColoniaTrabajo" value="{{ request('CasoCreateSolicitanteColoniaTrabajo', $resultado_socio->get('ColoniaTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteDelegacionTrabajoHasError'] or ''}}">
				<label for="CasoCreateSolicitanteDelegacionTrabajo" class="col-form-label col-sm-2">Delegacion Trabajo</label>
				<div class="col-sm-6 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteDelegacionTrabajo" name="CasoCreateSolicitanteDelegacionTrabajo" value="{{ request('CasoCreateSolicitanteDelegacionTrabajo', $resultado_socio->get('DelegacionTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitantePoblacionTrabajoHasError'] or ''}}">
				<label for="CasoCreateSolicitantePoblacionTrabajo" class="col-form-label col-sm-2">Ciudad Trabajo</label>
				<div class="col-sm-6 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitantePoblacionTrabajo" name="CasoCreateSolicitantePoblacionTrabajo" value="{{ request('CasoCreateSolicitantePoblacionTrabajo', $resultado_socio->get('PoblacionTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteEstadoTrabajoHasError'] or ''}}">
				<label for="CasoCreateSolicitanteEstadoTrabajo" class="col-form-label col-sm-2">Estado Trabajo</label>
				<div class="col-sm-6 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteEstadoTrabajo" name="CasoCreateSolicitanteEstadoTrabajo" value="{{ request('CasoCreateSolicitanteEstadoTrabajo', $resultado_socio->get('EstadoTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitantePaisTrabajoHasError'] or ''}}">
				<label for="CasoCreateSolicitantePaisTrabajo" class="col-form-label col-sm-2">Pais Trabajo</label>
				<div class="col-sm-4 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitantePaisTrabajo" name="CasoCreateSolicitantePaisTrabajo" value="{{ request('CasoCreateSolicitantePaisTrabajo', $resultado_socio->get('PaisTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteCodigoPostalTrabajoHasError'] or ''}}">
				<label for="CasoCreateSolicitanteCodigoPostalTrabajo" class="col-form-label col-sm-3">Codigo Postal Trabajo</label>
				<div class="col-sm-2">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteCodigoPostalTrabajo" name="CasoCreateSolicitanteCodigoPostalTrabajo" value="{{ request('CasoCreateSolicitanteCodigoPostalTrabajo', $resultado_socio->get('CodigoPostalTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteTelefonoTrabajoHasError'] or ''}}">
				<label for="CasoCreateSolicitanteTelefonoTrabajo" class="col-form-label col-sm-2">Telefono Trabajo</label>
				<div class="col-sm-4 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteTelefonoTrabajo" name="CasoCreateSolicitanteTelefonoTrabajo" value="{{ request('CasoCreateSolicitanteTelefonoTrabajo', $resultado_socio->get('TelefonoTrabajo')) }}" />
				</div>
			</div>
		</fieldset>
 
		<div class="btn-group pull-left"> 
	         <a href="#" class="btn btn-primary" onclick="$('#CasoCreatePhase').val(1);this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Anterior</a>
	    </div>
		<div class="btn-group pull-right"> 
	         <a href="#" class="btn btn-primary" onclick="this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>Siguiente</a>
	    </div>
	
	</form>
</div>
@endsection

@section('js')
@endsection