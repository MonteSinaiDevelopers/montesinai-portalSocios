@extends('layouts.app')

@section('header')
@endsection

@section('content')
<div class="container">
	<h1>Solicitud de Nuevo Caso</h1>
	
	<form id="CasoCreateForm" method="POST" action="{{ route('caso/new_submit') }}" >
		{{ csrf_field() }}
		<input type="hidden" name="CasoCreateAcceso" id="CasoCreateAcceso" value="{{Session::get('acceso', '0')}}" />
		<input type="hidden" name="CasoCreatePhase" id="CasoCreatePhase" value="8" />

		<div class="row">
			<div class="btn-group pull-left"> 
		         <a href="#" class="btn btn-primary" onclick="$('#CasoCreatePhase').val(7);this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Anterior</a>
		    </div>

			<div class="btn-group pull-right"> 
		         <a href="#" class="btn btn-success" onclick="this.disabled=true;$('#CasoCreateForm').submit();"></span>Enviar Solicitud</a>
		    </div>
		</div>

	    <div class="row">
		    <div class="col-sm-6">
				<div class="panel panel-default">
				  <div class="panel-heading">
				    <h3 class="panel-title">Solicitante</h3>
				  </div>
				  <div class="panel-body">
					<p>Nombre: {{ session('CasoNew.CasoCreateSolicitanteNombre', '') }} {{ session('CasoNew.CasoCreateSolicitanteApellidoPaterno', '') }} {{ session('CasoNew.CasoCreateSolicitanteApellidoMaterno', '') }}</p>
					<p>Fecha de Nacimiento: {{ date('d-M-Y', strtotime(session('CasoNew.CasoCreateSolicitanteFechaNac', '')))}}</p>
					<p>Tel. Contacto: {{ session('CasoNew.CasoCreateSolicitanteTelefonoContacto', '') }}</p>
					<p>E-Mail: {{ session('CasoNew.CasoCreateSolicitanteCorreo', '') }}</p>
					<p>Direccion: </p>
					<p>{{ session('CasoNew.CasoCreateSolicitanteDireccionCasa', '') }}</p>
					<p>{{ session('CasoNew.CasoCreateSolicitanteColoniaCasa', '') }}, {{ session('CasoNew.CasoCreateSolicitantePoblacionCasa', '') }}</p>
					<p>{{ session('CasoNew.CasoCreateSolicitanteDelegacionCasa', '') }}, {{ session('CasoNew.CasoCreateSolicitanteEstadoCasa', '') }}, {{ session('CasoNew.CasoCreateSolicitantePaisCasa', '') }}
					<p>C.P. {{ session('CasoNew.CasoCreateSolicitanteCodigoPostalCasa', '') }}. Tel. {{ session('CasoNew.CasoCreateSolicitanteTelefonoCasa', '') }}
				  </div>
				</div>
		    </div>

		    <div class="col-sm-6">
				<div class="panel panel-default">
				  <div class="panel-heading">
				    <h3 class="panel-title">Contraparte</h3>
				  </div>
				  <div class="panel-body">
					<p>Nombre: {{ session('CasoNew.CasoCreateContraparteNombre', '') }} {{ session('CasoNew.CasoCreateContraparteApellidoPaterno', '') }} {{ session('CasoNew.CasoCreateContraparteApellidoMaterno', '') }}</p>
					<p>Fecha de Nacimiento: {{ date('d-M-Y', strtotime(session('CasoNew.CasoCreateContraparteFechaNac', '')))}}</p>
					<p>Tel. Contacto: {{ session('CasoNew.CasoCreateContraparteTelefonoContacto', '') }}</p>
					<p>E-Mail: {{ session('CasoNew.CasoCreateContraparteCorreo', '') }}</p>
					<p>Direccion: </p>
					<p>{{ session('CasoNew.CasoCreateContraparteDireccionCasa', '') }}</p>
					<p>{{ session('CasoNew.CasoCreateContraparteColoniaCasa', '') }}, {{ session('CasoNew.CasoCreateContrapartePoblacionCasa', '') }}</p>
					<p>{{ session('CasoNew.CasoCreateContraparteDelegacionCasa', '') }}, {{ session('CasoNew.CasoCreateContraparteEstadoCasa', '') }}, {{ session('CasoNew.CasoCreateContrapartePaisCasa', '') }}
					<p>C.P. {{ session('CasoNew.CasoCreateContraparteCodigoPostalCasa', '') }}. Tel. {{ session('CasoNew.CasoCreateContraparteTelefonoCasa', '') }}
				  </div>
				</div>
		    </div>

		    <div class="col-sm-12">
				<div class="panel panel-default">
				  <div class="panel-heading">
				    <h3 class="panel-title">Tema a tratar</h3>
				  </div>
				  <div class="panel-body">
				  	<p>{!! nl2br(session('CasoNew.CasoCreateCasoDetalles', '')) !!}</p>
				  </div>
				</div>
		    </div>
		</div>

		<div class="row">
			<div class="btn-group pull-left"> 
		         <a href="#" class="btn btn-primary" onclick="$('#CasoCreatePhase').val(7);this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Anterior</a>
		    </div>

			<div class="btn-group pull-right"> 
		         <a href="#" class="btn btn-success" onclick="this.disabled=true;$('#CasoCreateForm').submit();"></span>Enviar Solicitud</a>
		    </div>
		</div>
	
	</form>
</div>
@endsection

@section('js')
@endsection