@extends('layouts.app')

@section('header')
@endsection

@section('content')
<div class="container">
	<h1>Solicitud de Nuevo Caso</h1>
	
	<form id="CasoCreateForm" method="POST" action="{{ route('caso/new_submit') }}" >
		{{ csrf_field() }}
		<input type="hidden" name="CasoCreateAcceso" id="CasoCreateAcceso" value="{{Session::get('acceso', '0')}}" />
		<input type="hidden" name="CasoCreatePhase" id="CasoCreatePhase" value="1" />
		<div class="btn-group pull-right"> 
	         <a href="#" class="btn btn-primary" onclick="this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>Siguiente</a>
	    </div>

		<!-- DATOS SOLICITANTE -->
		<fieldset>
			<div class="row">
				<h2>Solicitante</h2>
			</div>
			<div class="row">
				<div class="alert alert-warning" role="alert">
				  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				  <span class="sr-only">Advertencia</span>
				  Todos los campos son obligatorios.
				</div>
			</div>
			<div class="row">
				<h3>Datos Personales</h3>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteNombreHasError'] or ''}}">
				<label for="CasoCreateSolicitanteNombre" class="col-form-label col-sm-1">Nombre(s)</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteNombre" name="CasoCreateSolicitanteNombre" value="{{ request('CasoCreateSolicitanteNombre', $resultado_socio->get('Nombre')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteApellidoPaternoHasError'] or ''}}">
				<label for="CasoCreateSolicitanteApellidoPaterno" class="col-form-label col-sm-2">Apellido Paterno</label>
				<div class="col-sm-10">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteApellidoPaterno" name="CasoCreateSolicitanteApellidoPaterno" value="{{ request('CasoCreateSolicitanteApellidoPaterno', $resultado_socio->get('APaterno')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteApellidoMaternoHasError'] or ''}}">
				<label for="CasoCreateSolicitanteApellidoMaterno" class="col-form-label col-sm-2">Apellido Materno</label>
				<div class="col-sm-10">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteApellidoMaterno" name="CasoCreateSolicitanteApellidoMaterno" value="{{ request('CasoCreateSolicitanteApellidoMaterno', $resultado_socio->get('AMaterno')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteFechaNacHasError'] or ''}}">
				<label for="CasoCreateSolicitanteFechaNac" class="col-form-label col-sm-2">Fecha de Nacimiento</label>
				<div class="col-sm-10">
					<input type="date" class="form-control {{$form_errors['CasoCreateSolicitanteFechaNacHasError'] or ''}}" id="CasoCreateSolicitanteFechaNac" name="CasoCreateSolicitanteFechaNac" value="{{ request('CasoCreateSolicitanteFechaNac', date('Y-m-d', strtotime($resultado_socio->get('FechaNacimiento')))) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteTelefonoContactoHasError'] or ''}}">
				<label for="CasoCreateSolicitanteTelefonoContacto" class="col-form-label col-sm-2">Telefono de Contacto</label>
				<div class="col-sm-4">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteTelefonoContacto" name="CasoCreateSolicitanteTelefonoContacto" value="{{ request('CasoCreateSolicitanteTelefonoContacto', $resultado_socio->get('TelefonoCelular')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteCorreoHasError'] or ''}}">
				<label for="CasoCreateSolicitanteCorreo" class="col-form-label col-sm-1">Correo</label>
				<div class="col-sm-4 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteCorreo" name="CasoCreateSolicitanteCorreo" value="{{ request('CasoCreateSolicitanteCorreo', $resultado_socio->get('EMail')) }}" />
				</div>
			</div>
		</fieldset>
		 
		 <fieldset>
		 	<div class="row">
		 		<h3>Datos de casa</h3>
		 	</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteDireccionCasaHasError'] or ''}}">
				<label for="CasoCreateSolicitanteDireccionCasa" class="col-form-label col-sm-1">Direccion</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteDireccionCasa" name="CasoCreateSolicitanteDireccionCasa" value="{{ request('CasoCreateSolicitanteDireccionCasa', $resultado_socio->get('Direccion')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteColoniaCasaHasError'] or ''}}">
				<label for="CasoCreateSolicitanteColoniaCasa" class="col-form-label col-sm-2">Colonia</label>
				<div class="col-sm-6">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteColoniaCasa" name="CasoCreateSolicitanteColoniaCasa" value="{{ request('CasoCreateSolicitanteColoniaCasa', $resultado_socio->get('Colonia')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteDelegacionCasaHasError'] or ''}}">
				<label for="CasoCreateSolicitanteDelegacionCasa" class="col-form-label col-sm-2">Delegacion/Municipio</label>
				<div class="col-sm-6">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteDelegacionCasa" name="CasoCreateSolicitanteDelegacionCasa" value="{{ request('CasoCreateSolicitanteDelegacionCasa', $resultado_socio->get('Delegacion')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitantePoblacionCasaHasError'] or ''}}">
				<label for="CasoCreateSolicitantePoblacionCasa" class="col-form-label col-sm-1">Ciudad</label>
				<div class="col-sm-6 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitantePoblacionCasa" name="CasoCreateSolicitantePoblacionCasa" value="{{ request('CasoCreateSolicitantePoblacionCasa', $resultado_socio->get('Poblacion')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteEstadoCasaHasError'] or ''}}">
				<label for="CasoCreateSolicitanteEstadoCasa" class="col-form-label col-sm-1">Estado</label>
				<div class="col-sm-6 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteEstadoCasa" name="CasoCreateSolicitanteEstadoCasa" value="{{ request('CasoCreateSolicitanteEstadoCasa', $resultado_socio->get('Estado')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitantePaisCasaHasError'] or ''}}">
				<label for="CasoCreateSolicitantePaisCasa" class="col-form-label col-sm-1">Pais</label>
				<div class="col-sm-4 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitantePaisCasa" name="CasoCreateSolicitantePaisCasa" value="{{ request('CasoCreateSolicitantePaisCasa', $resultado_socio->get('Pais')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteCodigoPostalCasaHasError'] or ''}}">
				<label for="CasoCreateSolicitanteCodigoPostalCasa" class="col-form-label col-sm-2">Código Postal</label>
				<div class="col-sm-2">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteCodigoPostalCasa" name="CasoCreateSolicitanteCodigoPostalCasa" value="{{ request('CasoCreateSolicitanteCodigoPostalCasa', $resultado_socio->get('CodigoPostal')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteTelefonoCasaHasError'] or ''}}">
				<label for="CasoCreateSolicitanteTelefonoCasa" class="col-form-label col-sm-2">Teléfono de Casa</label>
				<div class="col-sm-4">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteTelefonoCasa" name="CasoCreateSolicitanteTelefonoCasa" value="{{ request('CasoCreateSolicitanteTelefonoCasa', $resultado_socio->get('Telefonos')) }}" />
				</div>
			</div>
		 </fieldset>

		<div class="btn-group pull-right"> 
	         <a href="#" class="btn btn-primary" onclick="this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>Siguiente</a>
	    </div>
	
	</form>
</div>
@endsection

@section('js')
@endsection