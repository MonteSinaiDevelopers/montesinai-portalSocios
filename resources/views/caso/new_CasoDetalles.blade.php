@extends('layouts.app')

@section('header')
@endsection

@section('content')
<div class="container">
	<h1>Solicitud de Nuevo Caso</h1>
	
	<form id="CasoCreateForm" method="POST" action="{{ route('caso/new_submit') }}" >
		{{ csrf_field() }}
		<input type="hidden" name="CasoCreateAcceso" id="CasoCreateAcceso" value="{{Session::get('acceso', '0')}}" />
		<input type="hidden" name="CasoCreatePhase" id="CasoCreatePhase" value="7" />

		<div class="btn-group pull-left"> 
	         <a href="#" class="btn btn-primary" onclick="$('#CasoCreatePhase').val(6);this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Anterior</a>
	    </div>
		<div class="btn-group pull-right"> 
	         <a href="#" class="btn btn-primary" onclick="this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>Siguiente</a>
	    </div>

		<fieldset>
			<div class="row">
				<h2>Tema a tratar</h2>
			</div>

			<div class="form-group row {{$form_errors['CasoCreateCasoDetalles'] or ''}}">
				<div class="col-sm-10 col-sm-offset-1">
					<textarea rows="20" name="CasoCreateCasoDetalles" id="CasoCreateCasoDetalles" class="form-control">{{ request('CasoCreateCasoDetalles', '') }}</textarea>
				</div>
			</div>

		</fieldset>

		<div class="btn-group pull-left"> 
	         <a href="#" class="btn btn-primary" onclick="$('#CasoCreatePhase').val(6);this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Anterior</a>
	    </div>
		<div class="btn-group pull-right"> 
	         <a href="#" class="btn btn-primary" onclick="this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>Siguiente</a>
	    </div>
	
	</form>
</div>
@endsection

@section('js')
@endsection