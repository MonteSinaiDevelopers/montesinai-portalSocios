@extends('layouts.app')
@section('header')

@endsection

@section('content')
<div class="container">
	<ul class="nav nav-tabs" id="CasoViewTab" role="tablist">
		<li class="nav-item active">
			<a class="nav-link active" id="casodetails-tab" data-toggle="tab" href="#casodetails" role="tab" aria-controls="casodetails" aria-expanded="true">Detalles</a>
		</li>
	</ul>
	<div class="tab-content" id="CasoViewTabContent">
		<div class="tab-pane active" id="casodetails" role="tabpanel" aria-labelledby="casodetails-tab">
			<div class="panel panel-primary">
				<div class="panel-heading">Tema a Tratar</div>

				<div class="panel-body">
					{{ $caso[0]['@Detalles'] }}
				</div>
				<div class="panel-heading">Documentos</div>
				<ul class="list-group" id="CasoViewAnexos">
					<li class="list-group-item">Cargando Anexos...</li>
					<li class="list-group-item">Fecha de Emision: {{ Date::parse(strtotime($caso[0]['@FechaEmision']))->format('l j F Y') }}</li>
				</ul>
					<li class="list-group-item">Atendido por: 
					<form>
					<select size="10" class="form-control" id="CasoViewResponsable1" name="CasoViewResponsable1">
						<option value=""></option>
						@forelse  ($directivos as $dir)
						<option value="{{ $dir['@ID'] }}" {{ ($caso[0]['@ResponsableID']==$dir['@ID'])? 'selected':'' }}>{{ $dir['@Nombre']}} ({{$dir['@CasosActivos']}})</option>
						@empty
						<option value="">No se encontraron directivos Activos</option>
						@endforelse
					</select>
					<input type="button" name="CasoViewAsignar1" id="CasoViewAsignar1" class="form-control btn-success CasoViewAsignar" value="Asignar Responsable" />
					</form>
					<li class="list-group-item">Asistido por: 
					<form>
					<select size="10" class="form-control" id="CasoViewResponsable2" name="CasoViewResponsable2">
						<option value=""></option>
						@forelse  ($directivos as $dir)
						<option value="{{ $dir['@ID'] }}" {{ ($caso[0]['@ResponsableID']==$dir['@ID'])? 'selected':'' }}>{{ $dir['@Nombre']}} ({{$dir['@CasosActivos2']}})</option>
						@empty
						<option value="">No se encontraron directivos Activos</option>
						@endforelse
					</select>
					<input type="button" name="CasoViewAsignar2" id="CasoViewAsignar2" class="form-control btn-success CasoViewAsignar" value="Asignar Asistido" />
					</form>
					</li>
				</ul>
				
				<div class="panel-body">
						<form>
							<div class="form-group row">
								<div class="col-sm-1">
									<label for="CasoViewEstatus" class="col-form-label">Estatus: </label>
								</div>
								<div class="col-sm-3">
									<select id="CasoViewEstatus" name="CasoViewEstatus" class="CasoViewEstatus form-control">
										<option value=""></option>
										<option value="VIGENTE" {{ ($caso[0]['@Estatus']=="VIGENTE")?"selected='selected'":"" }}>VIGENTE</option>
										<option value="PENDIENTE" {{ ($caso[0]['@Estatus']=="PENDIENTE")?"selected='selected'":"" }}>PENDIENTE</option>
										<option value="CANCELADO" {{ ($caso[0]['@Estatus']=="CANCELADO")?"selected='selected'":"" }}>RECHAZADO</option>
										<option value="CONCLUIDO" {{ ($caso[0]['@Estatus']=="CONCLUIDO")?"selected='selected'":"" }}>CONCLUIDO</option>
									</select>
								</div>
							</div>
						<textarea id="CasoViewConlusion" name="CasoViewConlusion" class="form-control">{{ $caso[0]['@Conclusion'] or '' }}</textarea>
						<input type="button" name="CasoViewActualizar" id="CasoViewActualizar" class="form-control btn btn-success" value="Actualizar" />
					</form>
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="panel panel-info">
					<div class="panel-heading">Solicitante</div>
				
					<ul class="list-group">
						<li class="list-group-item">Nombre: {{ $caso[0]['@SolicitanteNombre'] }} {{ $caso[0]['@SolicitanteApellidoPaterno'] }} {{ $caso[0]['@SolicitanteApellidoMaterno'] }}
						<li class="list-group-item">Telefono: {{ $caso[0]['@SolicitanteTelefonoContacto'] or 'N/A' }}</li>
						<li class="list-group-item">Correo: {{ $caso[0]['@SolicitanteCorreo'] or 'N/A' }}</li>  
						<li class="list-group-item">Comunidad: {{ $caso[0]['@SolicitanteComunidad'] or 'N/A' }}</li>  
					</ul>
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="panel panel-info">
					<div class="panel-heading">Contraparte</div>
					
					<ul class="list-group">
						<li class="list-group-item">Nombre: {{ $caso[0]['@ContraparteNombre'] }} {{ $caso[0]['@ContraparteApellidoPaterno'] }} {{ $caso[0]['@ContraparteApellidoMaterno'] or '' }}
						<li class="list-group-item">Telefono: {{ $caso[0]['@ContraparteTelefonoContacto'] or 'N/A' }}</li>
						<li class="list-group-item">Correo: {{ $caso[0]['@ContraparteCorreo'] or 'N/A' }}</li>  
						<li class="list-group-item">Comunidad: {{ $caso[0]['@ContraparteComunidad'] or 'N/A' }}</li>  
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')

$(document).ready(function(){
// cambiar manualmente al valor selected de cada select, para evitar que se cambie en refresh
$('.CasoViewAsignar').click(function(){
	var id_dir, id_obj;
	id_obj = $(this).attr('id');
	if(id_obj == 'CasoViewAsignar1'){
		id_dir = $('#CasoViewResponsable1').val();
	}
	else if(id_obj == 'CasoViewAsignar2'){
		id_dir = $('#CasoViewResponsable2').val();
	}

	$.getJSON(
		"{{ route('caso/asignarJSON') }}",
		{
			'id_caso': {{ request('casoid', 0) }},
			'obj': id_obj,
			'dir': id_dir
		},
		function(data){
			if(data.success == 0){
				doNotify(data.msg, 'error');
			}
			else {
				doNotify('Asignado', 'success');
			}
		}
	);
});

$('#CasoViewActualizar').click(function(){
	$.getJSON(
		"{{ route('caso/actualizarJSON') }}",
		{
			'idcaso': {{ request('casoid', 0) }},
			'Estatus': $('#CasoViewEstatus').val(),
			'Conclusion': $('#CasoViewConlusion').val(),
		},
		function(data){
			if(data.success == 0){
				doNotify(data.msg, 'error');
			}
			else {
				doNotify('Actualizado', 'success');
			}
		}
	);
});

$.getJSON(
	"{{ route('caso/getAnexosJSON') }}",
	{
		'IDCaso': {{ request('casoid', 0) }}
	},
	function(data){
		if(data.success == 1){
			$('ul#CasoViewAnexos li').remove();

			$('ul#CasoViewAnexos').append(Mustache.render('@{{#Anexos}}<li class="list-group-item"><a target="_blank" href="{{ route('caso/getAnexo') }}?IDR=@{{{@IDR}}}">@{{{@Nombre}}}</a></li>@{{/Anexos}}@{{^Anexos}}<li class="list-group-item">No hay documentos</li>@{{/Anexos}}', data));
		}
	});
});
@endsection