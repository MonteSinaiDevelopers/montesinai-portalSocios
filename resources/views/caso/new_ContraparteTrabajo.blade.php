@extends('layouts.app')

@section('header')
@endsection

@section('content')
<div class="container">
	<h1>Solicitud de Nuevo Caso</h1>
	
	<form id="CasoCreateForm" method="POST" action="{{ route('caso/new_submit') }}" >
		{{ csrf_field() }}
		<input type="hidden" name="CasoCreateAcceso" id="CasoCreateAcceso" value="{{Session::get('acceso', '0')}}" />
		<input type="hidden" name="CasoCreatePhase" id="CasoCreatePhase" value="5" />

		<div class="btn-group pull-left"> 
	         <a href="#" class="btn btn-primary" onclick="$('#CasoCreatePhase').val(4);this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Anterior</a>
	    </div>
		<div class="btn-group pull-right"> 
	         <a href="#" class="btn btn-primary" onclick="this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>Siguiente</a>
	    </div>

		<fieldset>
			<div class="row">
				<h2>Contraparte</h2>
			</div>
				<div class="alert alert-warning" role="alert">
				  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				  <span class="sr-only">Advertencia</span>
				  Los siguientes datos son obligatorios:
				  <ul>
				  		<li>Nombre del lugar de trabajo</li>
				  		<li>Puesto que desempeña la persona</li>
				  </ul>
				  Sin embargo, nos ayuda mucho que nos sea proporcionada la mayor cantidad de información.
				</div>
			<div class="row">
				<h3>Datos de Trabajo</h3>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteNombreTrabajoHasError'] or ''}}">
				<label for="CasoCreateContraparteNombreTrabajo" class="col-form-label col-sm-3">Nombre Lugar de Trabajo</label>
				<div class="col-sm-6">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteNombreTrabajo" name="CasoCreateContraparteNombreTrabajo" value="{{ request('CasoCreateContraparteNombreTrabajo', $resultado_socio->get('NombreTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContrapartePuestoHasError'] or ''}}">
				<label for="CasoCreateContrapartePuesto" class="col-form-label col-sm-1">Puesto</label>
				<div class="col-sm-4 col-sm-offset-2">
					<input type="text" size="50" class="form-control" id="CasoCreateContrapartePuesto" name="CasoCreateContrapartePuesto" value="{{ request('CasoCreateContrapartePuesto', $resultado_socio->get('Puesto')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteDireccionTrabajoHasError'] or ''}}">
				<label for="CasoCreateContraparteDireccionTrabajo" class="col-form-label col-sm-2">Direccion Trabajo</label>
				<div class="col-sm-9 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteDireccionTrabajo" name="CasoCreateContraparteDireccionTrabajo" value="{{ request('CasoCreateContraparteDireccionTrabajo', $resultado_socio->get('DireccionTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteColoniaTrabajoHasError'] or ''}}">
				<label for="CasoCreateContraparteColoniaTrabajo" class="col-form-label col-sm-2">Colonia Trabajo</label>
				<div class="col-sm-6 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteColoniaTrabajo" name="CasoCreateContraparteColoniaTrabajo" value="{{ request('CasoCreateContraparteColoniaTrabajo', $resultado_socio->get('ColoniaTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteDelegacionTrabajoHasError'] or ''}}">
				<label for="CasoCreateContraparteDelegacionTrabajo" class="col-form-label col-sm-2">Delegacion Trabajo</label>
				<div class="col-sm-6 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteDelegacionTrabajo" name="CasoCreateContraparteDelegacionTrabajo" value="{{ request('CasoCreateContraparteDelegacionTrabajo', $resultado_socio->get('DelegacionTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContrapartePoblacionTrabajoHasError'] or ''}}">
				<label for="CasoCreateContrapartePoblacionTrabajo" class="col-form-label col-sm-2">Ciudad Trabajo</label>
				<div class="col-sm-6 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContrapartePoblacionTrabajo" name="CasoCreateContrapartePoblacionTrabajo" value="{{ request('CasoCreateContrapartePoblacionTrabajo', $resultado_socio->get('PoblacionTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteEstadoTrabajoHasError'] or ''}}">
				<label for="CasoCreateContraparteEstadoTrabajo" class="col-form-label col-sm-2">Estado Trabajo</label>
				<div class="col-sm-6 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteEstadoTrabajo" name="CasoCreateContraparteEstadoTrabajo" value="{{ request('CasoCreateContraparteEstadoTrabajo', $resultado_socio->get('EstadoTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContrapartePaisTrabajoHasError'] or ''}}">
				<label for="CasoCreateContrapartePaisTrabajo" class="col-form-label col-sm-2">Pais Trabajo</label>
				<div class="col-sm-4 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContrapartePaisTrabajo" name="CasoCreateContrapartePaisTrabajo" value="{{ request('CasoCreateContrapartePaisTrabajo', $resultado_socio->get('PaisTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteCodigoPostalTrabajoHasError'] or ''}}">
				<label for="CasoCreateContraparteCodigoPostalTrabajo" class="col-form-label col-sm-3">Codigo Postal Trabajo</label>
				<div class="col-sm-2">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteCodigoPostalTrabajo" name="CasoCreateContraparteCodigoPostalTrabajo" value="{{ request('CasoCreateContraparteCodigoPostalTrabajo', $resultado_socio->get('CodigoPostalTrabajo')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteTelefonoTrabajoHasError'] or ''}}">
				<label for="CasoCreateContraparteTelefonoTrabajo" class="col-form-label col-sm-2">Telefono Trabajo</label>
				<div class="col-sm-4 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteTelefonoTrabajo" name="CasoCreateContraparteTelefonoTrabajo" value="{{ request('CasoCreateContraparteTelefonoTrabajo', $resultado_socio->get('TelefonoTrabajo')) }}" />
				</div>
			</div>
		</fieldset>
 
		<div class="btn-group pull-left"> 
	         <a href="#" class="btn btn-primary" onclick="$('#CasoCreatePhase').val(5);this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Anterior</a>
	    </div>
		<div class="btn-group pull-right"> 
	         <a href="#" class="btn btn-primary" onclick="this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>Siguiente</a>
	    </div>
	
	</form>
</div>
@endsection

@section('js')
@endsection