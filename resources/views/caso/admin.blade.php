@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                	<h3 class="panel-title pull-left">Casos Ahavat Shalom</h3>

                	<button class="btn btn-success pull-right" id="CasoAdminRefresh">Refrescar</button>
                </div>

                <div class="panel-body">
                    <table class="CasoAdminTable table table-hover table-striped table-sm" id="CasoAdminTable">
                    	<thead>
                    		<tr>
                    			<th class="col-sm-1 text-center datatables-filtercolumn-select">Año</th>
	                    		<th class="col-sm-1 text-center">ID</th>
	                    		<th class="col-sm-3 text-center">Solicitante</th>
	                    		<th class="col-sm-3 text-center datatables-filtercolumn-select">Comunidad</th>
	                    		<th class="col-sm-3 text-center">Contraparte</th>
	                    		<th class="col-sm-3 text-center datatables-filtercolumn-select">Comunidad</th>
	                    		<th class="col-sm-2 text-center datatables-filtercolumn-select">Categoria</th>
	                    		<th class="col-sm-2 text-center datatables-filtercolumn-select">Responsable</th>
	                    		<th class="col-sm-2 text-center">Fecha</th>
	                    		<th class="col-sm-2 text-center datatables-filtercolumn-select">Estatus</th>
	                    		<th class="col-sm-1 text-center"></th>
	                    	</tr>
                    	</thead>
                    	<tbody>
                    		<tr>
                    			<td colspan="11">Cargando tabla...</td>
                    		</tr>
                    	</tbody>
                    	<tfoot>
                    		<tr>
                    			<th class="col-sm-1 text-center datatables-filtercolumn">Año</th>
	                    		<th class="col-sm-1 text-center">ID</th>
	                    		<th class="col-sm-2 text-center">Solicitante</th>
	                    		<th class="col-sm-3 text-center datatables-filtercolumn-select">Comunidad</th>
	                    		<th class="col-sm-3 text-center">Contraparte</th>
	                    		<th class="col-sm-3 text-center datatables-filtercolumn-select">Comunidad</th>
	                    		<th class="col-sm-2 text-center datatables-filtercolumn-select">Categoria</th>
	                    		<th class="col-sm-2 text-center datatables-filtercolumn">Responsable</th>
	                    		<th class="col-sm-2 text-center">Fecha</th>
	                    		<th class="col-sm-2 text-center datatables-filtercolumn">Estatus</th>
	                    		<th class="col-sm-1 text-center"></th>
	                    	</tr>
                    	</tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
function fillCasoList(force){
    if(force === undefined) {
        force = 0;
    }
	$.getJSON(
		"{{ route('caso/getJSON') }}",
		{
			'force': force
		},
		function(data){
			$('#CasoAdminTable > tbody').empty();

			data.Fecha_format = function(){
				return formatDateSpanish(new Date(this['@FechaEmision']));
			};

			data.Year = function() {
				var d = new Date(this['@FechaEmision']);
				return d.getFullYear();
			};

			data.urls = {};
			data.labels = {};
			data.urls.viewurl = '{{route('caso/get')}}';
			data.urls.deleteurl = '{{route('caso/deleteJSON')}}';
			data.labels.viewlabel = '{{ __('common.view_action') }}'
			data.labels.deletelabel = '{{ __('common.delete_action') }}'

			$.get({ url: "../resources/views/mustaches/caso/casolistitem.html", cache: false, success: function(template){
					$('#CasoAdminTable > tbody').mFill(data, {'template': template});

					if ( $.fn.dataTable.isDataTable( '#CasoAdminTable' ) ) {
						table = $('#CasoAdminTable').DataTable( { retrieve: true } );
    					table.destroy();
					}

					$('#CasoAdminTable').DataTable({
						initComplete: function () {
				            this.api().columns('.datatables-filtercolumn-select').every( function () {
				                var column = this;
				                var select = $('<select id="datatables-filtercolumn-select'+column.index()+'"><option value="">({{ __('common.all-m') }})</option></select>')
									.on( 'click' , function (evt){
									     evt.stopPropagation();
									 })
				                    .appendTo( $(column.header()) )
				                    .on( 'change', function () {
				                        var val = $.fn.dataTable.util.escapeRegex(
				                            $(this).val()
				                        );
				 
				                        column
				                            .search( val ? '^'+val+'$' : '', true, false )
				                            .draw();
				                    } );
				 
				                column.data().unique().sort().each( function ( d, j ) {
				                	if(d.trim() == ''){
				                		d = '({{ __('common.empty') }})';
				                	}
				                    select.append( '<option value="'+d+'">'+d+'</option>' )
				                } );
				            } );
						}
					});
				}
			});
		} // function(data)
	);
}

$(document).ready(function(){
	fillCasoList();

	$('#CasoAdminRefresh').click(function(){
		fillCasoList(1);
	});
});
@endsection