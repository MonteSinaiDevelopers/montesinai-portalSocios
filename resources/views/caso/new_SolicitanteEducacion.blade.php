@extends('layouts.app')

@section('header')
@endsection

@section('content')
<div class="container">
	<h1>Solicitud de Nuevo Caso</h1>
	
	<form id="CasoCreateForm" method="POST" action="{{ route('caso/new_submit') }}" >
		{{ csrf_field() }}
		<input type="hidden" name="CasoCreateAcceso" id="CasoCreateAcceso" value="{{Session::get('acceso', '0')}}" />
		<input type="hidden" name="CasoCreatePhase" id="CasoCreatePhase" value="3" />

		<div class="btn-group pull-left"> 
	         <a href="#" class="btn btn-primary" onclick="$('#CasoCreatePhase').val(2);this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Anterior</a>
	    </div>
		<div class="btn-group pull-right"> 
	         <a href="#" class="btn btn-primary" onclick="this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>Siguiente</a>
	    </div>

		<fieldset>
			<div class="row">
				<h2>Solicitante</h2>
			</div>
			<div class="row">
				<div class="alert alert-warning" role="alert">
				  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				  <span class="sr-only">Advertencia</span>
				  Todos los campos son obligatorios.
				</div>
			</div>
			<div class="row">
				<h3>Datos de Educacion</h3>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteEscKinderHasError'] or ''}}">
				<label for="CasoCreateSolicitanteEscKinder" class="col-form-label col-sm-1">Kinder</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteEscKinder" name="CasoCreateSolicitanteEscKinder" value="{{ request('CasoCreateSolicitanteEscKinder', $resultado_socio->get('Nombre')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteEscPrimariaHasError'] or ''}}">
				<label for="CasoCreateSolicitanteEscPrimaria" class="col-form-label col-sm-1">Primaria</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteEscPrimaria" name="CasoCreateSolicitanteEscPrimaria" value="{{ request('CasoCreateSolicitanteEscPrimaria', $resultado_socio->get('Nombre')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteEscSecundariaHasError'] or ''}}">
				<label for="CasoCreateSolicitanteEscSecundaria" class="col-form-label col-sm-1">Secundaria</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteEscSecundaria" name="CasoCreateSolicitanteEscSecundaria" value="{{ request('CasoCreateSolicitanteEscSecundaria', $resultado_socio->get('Nombre')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteEscPreparatoriaHasError'] or ''}}">
				<label for="CasoCreateSolicitanteEscPreparatoria" class="col-form-label col-sm-1">Preparatoria</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteEscPreparatoria" name="CasoCreateSolicitanteEscPreparatoria" value="{{ request('CasoCreateSolicitanteEscPreparatoria', $resultado_socio->get('Nombre')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteEscUniversidadHasError'] or ''}}">
				<label for="CasoCreateSolicitanteEscUniversidad" class="col-form-label col-sm-1">Universidad</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteEscUniversidad" name="CasoCreateSolicitanteEscUniversidad" value="{{ request('CasoCreateSolicitanteEscUniversidad', $resultado_socio->get('Nombre')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteEscCarreraHasError'] or ''}}">
				<label for="CasoCreateSolicitanteEscCarrera" class="col-form-label col-sm-1">Carrera</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteEscCarrera" name="CasoCreateSolicitanteEscCarrera" value="{{ request('CasoCreateSolicitanteEscCarrera', $resultado_socio->get('Nombre')) }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateSolicitanteEscOtrosHasError'] or ''}}">
				<label for="CasoCreateSolicitanteEscOtros" class="col-form-label col-sm-1">Otros</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateSolicitanteEscOtros" name="CasoCreateSolicitanteEscOtros" value="{{ request('CasoCreateSolicitanteEscOtros', $resultado_socio->get('Nombre')) }}" />
				</div>
			</div>
		</fieldset>

		<div class="btn-group pull-left"> 
	         <a href="#" class="btn btn-primary" onclick="$('#CasoCreatePhase').val(2);this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Anterior</a>
	    </div>
		<div class="btn-group pull-right"> 
	         <a href="#" class="btn btn-primary" onclick="this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>Siguiente</a>
	    </div>
	
	</form>
</div>
@endsection

@section('js')
@endsection