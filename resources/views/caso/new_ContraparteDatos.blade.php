
@extends('layouts.app')

@section('header')
@endsection

@section('content')
<div class="container">
	<h1>Solicitud de Nuevo Caso</h1>
	
	<form id="CasoCreateForm" method="POST" action="{{ route('caso/new_submit') }}" >
		{{ csrf_field() }}
		<input type="hidden" name="CasoCreateAcceso" id="CasoCreateAcceso" value="{{Session::get('acceso', '0')}}" />
		<input type="hidden" name="CasoCreatePhase" id="CasoCreatePhase" value="4" />

		<div class="btn-group pull-left"> 
	         <a href="#" class="btn btn-primary" onclick="$('#CasoCreatePhase').val(3);this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Anterior</a>
	    </div>
		<div class="btn-group pull-right"> 
	         <a href="#" class="btn btn-primary" onclick="this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>Siguiente</a>
	    </div>

		<fieldset>
			<div class="row">
				<h2>Contraparte</h2>
			</div>
			<div class="row">
				<div class="alert alert-warning" role="alert">
				  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				  <span class="sr-only">Advertencia</span>
				  Los siguientes datos son obligatorios:
				  <ul>
				  		<li>Nombre y Apellidos</li>
				  		<li>Telefono de Contacto</li>
				  </ul>
				  Sin embargo, nos ayuda mucho que nos sea proporcionada la mayor cantidad de información.
				</div>
			</div>
			<div class="row">
				<h3>Datos Personales</h3>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteNombreHasError'] or ''}}">
				<label for="CasoCreateContraparteNombre" class="col-form-label col-sm-1">Nombre(s)</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteNombre" name="CasoCreateContraparteNombre" value="{{ request('CasoCreateContraparteNombre', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteApellidoPaternoHasError'] or ''}}">
				<label for="CasoCreateContraparteApellidoPaterno" class="col-form-label col-sm-2">Apellido Paterno</label>
				<div class="col-sm-10">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteApellidoPaterno" name="CasoCreateContraparteApellidoPaterno" value="{{ request('CasoCreateContraparteApellidoPaterno', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteApellidoMaternoHasError'] or ''}}">
				<label for="CasoCreateContraparteApellidoMaterno" class="col-form-label col-sm-2">Apellido Materno</label>
				<div class="col-sm-10">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteApellidoMaterno" name="CasoCreateContraparteApellidoMaterno" value="{{ request('CasoCreateContraparteApellidoMaterno', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteFechaNacHasError'] or ''}}">
				<label for="CasoCreateContraparteFechaNac" class="col-form-label col-sm-2">Fecha de Nacimiento</label>
				<div class="col-sm-10">
					<input type="date" class="form-control {{$form_errors['CasoCreateContraparteFechaNacHasError'] or ''}}" id="CasoCreateContraparteFechaNac" name="CasoCreateContraparteFechaNac" value="{{ request('CasoCreateContraparteFechaNac', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteTelefonoContactoHasError'] or ''}}">
				<label for="CasoCreateContraparteTelefonoContacto" class="col-form-label col-sm-2">Telefono de Contacto</label>
				<div class="col-sm-4">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteTelefonoContacto" name="CasoCreateContraparteTelefonoContacto" value="{{ request('CasoCreateContraparteTelefonoContacto', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteCorreoHasError'] or ''}}">
				<label for="CasoCreateContraparteCorreo" class="col-form-label col-sm-1">Correo</label>
				<div class="col-sm-4 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteCorreo" name="CasoCreateContraparteCorreo" value="{{ request('CasoCreateContraparteCorreo', '') }}" />
				</div>
			</div>
		</fieldset>
		 
		 <fieldset>
		 	<div class="row">
		 		<h3>Datos de casa</h3>
		 	</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteDireccionCasaHasError'] or ''}}">
				<label for="CasoCreateContraparteDireccionCasa" class="col-form-label col-sm-1">Direccion</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteDireccionCasa" name="CasoCreateContraparteDireccionCasa" value="{{ request('CasoCreateContraparteDireccionCasa', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteColoniaCasaHasError'] or ''}}">
				<label for="CasoCreateContraparteColoniaCasa" class="col-form-label col-sm-2">Colonia</label>
				<div class="col-sm-6">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteColoniaCasa" name="CasoCreateContraparteColoniaCasa" value="{{ request('CasoCreateContraparteColoniaCasa', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteDelegacionCasaHasError'] or ''}}">
				<label for="CasoCreateContraparteDelegacionCasa" class="col-form-label col-sm-2">Delegacion/Municipio</label>
				<div class="col-sm-6">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteDelegacionCasa" name="CasoCreateContraparteDelegacionCasa" value="{{ request('CasoCreateContraparteDelegacionCasa', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContrapartePoblacionCasaHasError'] or ''}}">
				<label for="CasoCreateContrapartePoblacionCasa" class="col-form-label col-sm-1">Ciudad</label>
				<div class="col-sm-6 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContrapartePoblacionCasa" name="CasoCreateContrapartePoblacionCasa" value="{{ request('CasoCreateContrapartePoblacionCasa', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteEstadoCasaHasError'] or ''}}">
				<label for="CasoCreateContraparteEstadoCasa" class="col-form-label col-sm-1">Estado</label>
				<div class="col-sm-6 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteEstadoCasa" name="CasoCreateContraparteEstadoCasa" value="{{ request('CasoCreateContraparteEstadoCasa', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContrapartePaisCasaHasError'] or ''}}">
				<label for="CasoCreateContrapartePaisCasa" class="col-form-label col-sm-1">Pais</label>
				<div class="col-sm-4 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContrapartePaisCasa" name="CasoCreateContrapartePaisCasa" value="{{ request('CasoCreateContrapartePaisCasa', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteCodigoPostalCasaHasError'] or ''}}">
				<label for="CasoCreateContraparteCodigoPostalCasa" class="col-form-label col-sm-2">Código Postal</label>
				<div class="col-sm-2">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteCodigoPostalCasa" name="CasoCreateContraparteCodigoPostalCasa" value="{{ request('CasoCreateContraparteCodigoPostalCasa', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteTelefonoCasaHasError'] or ''}}">
				<label for="CasoCreateContraparteTelefonoCasa" class="col-form-label col-sm-2">Teléfono de Casa</label>
				<div class="col-sm-4">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteTelefonoCasa" name="CasoCreateContraparteTelefonoCasa" value="{{ request('CasoCreateContraparteTelefonoCasa', '') }}" />
				</div>
			</div>
		 </fieldset>

		<div class="btn-group pull-left"> 
	         <a href="#" class="btn btn-primary" onclick="$('#CasoCreatePhase').val(3);this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Anterior</a>
	    </div>
		<div class="btn-group pull-right"> 
	         <a href="#" class="btn btn-primary" onclick="this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>Siguiente</a>
	    </div>
	
	</form>
</div>
@endsection

@section('js')
@endsection