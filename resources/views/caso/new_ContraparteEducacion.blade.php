@extends('layouts.app')

@section('header')
@endsection

@section('content')
<div class="container">
	<h1>Solicitud de Nuevo Caso</h1>
	
	<form id="CasoCreateForm" method="POST" action="{{ route('caso/new_submit') }}" >
		{{ csrf_field() }}
		<input type="hidden" name="CasoCreateAcceso" id="CasoCreateAcceso" value="{{Session::get('acceso', '0')}}" />
		<input type="hidden" name="CasoCreatePhase" id="CasoCreatePhase" value="6" />

		<div class="btn-group pull-left"> 
	         <a href="#" class="btn btn-primary" onclick="$('#CasoCreatePhase').val(5);this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Anterior</a>
	    </div>
		<div class="btn-group pull-right"> 
	         <a href="#" class="btn btn-primary" onclick="this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>Siguiente</a>
	    </div>

		<fieldset>
			<div class="row">
				<h2>Contraparte</h2>
			</div>
				<div class="alert alert-info" role="alert">
				  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				  <span class="sr-only">Información</span>
				  Todos los datos son opcionales; sin embargo, nos ayuda mucho que nos sea proporcionada la mayor cantidad de información.
				</div>
			<div class="row">
				<h3>Datos de Educacion</h3>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteEscKinderHasError'] or ''}}">
				<label for="CasoCreateContraparteEscKinder" class="col-form-label col-sm-1">Kinder</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteEscKinder" name="CasoCreateContraparteEscKinder" value="{{ request('CasoCreateContraparteEscKinder', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteEscPrimariaHasError'] or ''}}">
				<label for="CasoCreateContraparteEscPrimaria" class="col-form-label col-sm-1">Primaria</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteEscPrimaria" name="CasoCreateContraparteEscPrimaria" value="{{ request('CasoCreateContraparteEscPrimaria', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteEscSecundariaHasError'] or ''}}">
				<label for="CasoCreateContraparteEscSecundaria" class="col-form-label col-sm-1">Secundaria</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteEscSecundaria" name="CasoCreateContraparteEscSecundaria" value="{{ request('CasoCreateContraparteEscSecundaria', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteEscPreparatoriaHasError'] or ''}}">
				<label for="CasoCreateContraparteEscPreparatoria" class="col-form-label col-sm-1">Preparatoria</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteEscPreparatoria" name="CasoCreateContraparteEscPreparatoria" value="{{ request('CasoCreateContraparteEscPreparatoria', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteEscUniversidadHasError'] or ''}}">
				<label for="CasoCreateContraparteEscUniversidad" class="col-form-label col-sm-1">Universidad</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteEscUniversidad" name="CasoCreateContraparteEscUniversidad" value="{{ request('CasoCreateContraparteEscUniversidad', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteEscCarreraHasError'] or ''}}">
				<label for="CasoCreateContraparteEscCarrera" class="col-form-label col-sm-1">Carrera</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteEscCarrera" name="CasoCreateContraparteEscCarrera" value="{{ request('CasoCreateContraparteEscCarrera', '') }}" />
				</div>
			</div>
			<div class="form-group row {{$form_errors['CasoCreateContraparteEscOtrosHasError'] or ''}}">
				<label for="CasoCreateContraparteEscOtros" class="col-form-label col-sm-1">Otros</label>
				<div class="col-sm-10 col-sm-offset-1">
					<input type="text" size="50" class="form-control" id="CasoCreateContraparteEscOtros" name="CasoCreateContraparteEscOtros" value="{{ request('CasoCreateContraparteEscOtros', '') }}" />
				</div>
			</div>
		</fieldset>

		<div class="btn-group pull-left"> 
	         <a href="#" class="btn btn-primary" onclick="$('#CasoCreatePhase').val(5);this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Anterior</a>
	    </div>
		<div class="btn-group pull-right"> 
	         <a href="#" class="btn btn-primary" onclick="this.disabled=true;$('#CasoCreateForm').submit();"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>Siguiente</a>
	    </div>
	
	</form>
</div>
@endsection

@section('js')
@endsection