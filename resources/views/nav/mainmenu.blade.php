<nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false"> <span class="sr-only">Navegacion</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a href="#" class="navbar-brand">Monte Sinai</a>
        </div>
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                @if(trim(session("acceso", "")) != "")
                <li><a href="{{ route('socio/getCxCPendientesAcceso') }}">Saldos Pendientes</a></li>
                <li><a href="{{ route('socio/getEstadoCuentaAcceso') }}">Estado de Cuenta</a></li>
                <li><a href="{{ route('socio/anexo') }}">Archivos CFDI</a></li>
                @endif
                @ability('administrator', 'read-users') <li><a href="{{ route('user/admin') }}">Usuarios</a></li> @endability
                @permission('read-socios') <li><a href="{{ route('socio/admin') }}">Socios</a></li> @endpermission
                @ability('directivo-casos', 'read-caso') <li><a href="{{ route('caso/admin') }}">Casos</a></li> @endability
                @if(Auth::user() != NULL && Auth::user()->can('read-casos'))
                <li><a href="{{route('caso/admin')}}">Casos</a></li>
                @endif
                @if(Session::has('EnProcesoBM_idk'))
                <li><a href="{{route('barmitzvah/new')}}?k={{Session::get('EnProcesoBM_idk')}}">Bar-Mitzvah</a></li>
                @endif
            </ul>
			
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <!-- <li><a href="{{ route('register') }}">Registrarse</a></li>-->
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            @role('user')<li><a href="{{ route('profile/edit') }}">Editar Perfil</a></li>@endrole
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
            
        </div>
    </div>
</nav>