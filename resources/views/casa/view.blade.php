@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Detalles Casa</div>
	                <div class="panel-body">
	                	<ul class="list-group col-sm-6 col-sm-offset-3">
	                		<li class="list-group-item">Telefonos: {{ $casa[0]['@Telefonos'] or '' }} {{ $casa[0]['@TelefonosSecundario'] or '' }}</li>
	                		<li class="list-group-item">Direccion: {{ $casa[0]['@Direccion'] or '' }}</li>
	                		<li class="list-group-item">Colonia: {{ $casa[0]['@Colonia'] or '' }}</li>
	                		<li class="list-group-item">Delegacion: {{ $casa[0]['@Delegacion'] or '' }}</li>
	                		<li class="list-group-item">Ciudad: {{ $casa[0]['@Poblacion'] or '' }}</li>
	                		<li class="list-group-item">Estado: {{ $casa[0]['@Estado'] or '' }}</li>
	                		<li class="list-group-item">Pais: {{ $casa[0]['@Pais'] or '' }}</li>
	                		<li class="list-group-item">Observaciones: {{ $casa[0]['@Observaciones'] or '' }}</li>
	                	</ul>
	                </div>

				<div class="panel-heading">Integrantes</div>
					<table class="table CasaViewSocios" id="CasaViewSocios">
						<thead>
							<tr>
								<td>ID Socio</td>
								<td>NoFamiliaAnt</td>
								<td>Nombre</td>
								<td>Estatus</td>
								<td>Sexo</td>
								<td>Estado Civil</td>
								<td>Celular</td>
								<td>EMail</td>
								<td>Profesion</td>
								<td>Nacimiento</td>
								<td>Matrimonio</td>
								<td>Comunidad</td>
								<td>Año Creden.</td>
								<td>&nbsp;</td>
							</tr>
						</thead>
						<tbody>
							@forelse($casa as $socio)
							<tr>
								<td>{{ $socio['@ID_Socio_Formato'] or '' }}</td>
								<td>{{ $socio['@No_FamiliaAnt'] or '' }}</td>
								<td>{{ $socio['@NombreCompleto'] or '' }}</td>
								<td>{{ $socio['@Estatus'] or '' }}</td>
								<td>{{ $socio['@Sexo'] or '' }}</td>
								<td>{{ $socio['@EstadoCivil'] or '' }}</td>
								<td>{{ $socio['@TelefonoCelular'] or '' }}</td>
								<td>{{ $socio['@EMail'] or '' }}</td>
								<td>{{ $socio['@Profesion'] or '' }}</td>
								<td>
									@if(isset($socio['@FechaNacimiento']) && $socio['@FechaNacimiento'] != '')
										{{ date('d/m/Y', strtotime($socio['@FechaNacimiento'])) }}
									@endif
								</td>
								<td>
									@if(isset($socio['@FechaMatrimonio']) && $socio['@FechaMatrimonio'] != '')
										{{ date('d/m/Y', strtotime($socio['@FechaMatrimonio'])) }}
									@endif
								</td>
								<td>{{ $socio['@ComunidadNombre'] or '' }}</td>
								<td>{{ $socio['@FechaCredencial'] or '' }}</td>
								@permission('read-socioscxc')
									<td>
										<a href="{{route('socio/getCxCPendientes')}}?id_socio={{ $socio['@ID_Socio'] }}">CxC</a> |
										<a href="{{route('socio/getEstadoCuenta')}}?id_socio={{ $socio['@ID_Socio'] }}">Hist</a>
									</td>
								@endpermission
							</tr>
							@empty
							<tr><td>No hay socios</td></tr>
							@endforelse
						</tbody>
					</table>
            </div>
        </div>
    </div>
</div>
@endsection