@extends('layouts.home')

@section('content')
<div class="container">

	<div class="jumbotron">
		<div class="row">
			<div class="col-sm-4">
				<img class="img-responsive" src="public/images/azul-msm.png" />
			</div>
			<div class="col-sm-8">
				<h2 class="text-center">Portal de Socios</h2>
				<h1 class="text-center">Comunidad Monte Sinai</h1>
			</div>
		</div>
	</div>
	
	<div class="panel panel-default">
		<div class="panel-body">
			<h1 class="text-center">Para obtener su acceso para consultas, favor de comunicarse al 5596-9966 ext. 105, 103 o 118 y tener lista una dirección de correo para recibir el aviso</h1>
		</div>
	</div>

</div>
@endsection
