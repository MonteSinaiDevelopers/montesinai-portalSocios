<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>

	<script src="{{ asset('public/js/jquery-3.1.0.min.js') }}"></script>
    <link href="{{ asset('public/css/bootstrap-theme.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">
	<script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
    <link href="{{url('/public')}}{{ mix('/css/home.css') }}" rel="stylesheet">
	@yield('header')
</head>
<body>
    <div id="app">
        @include('nav.mainmenu')

		@include('common.errors')
        @yield('content')
    </div>

    <!-- Scripts -->
    <script lang="text/javascript">
	//Activar Tabs. Tienen que tener la clase NavTabs
	$('.NavTabs a').click(function (e) {
		e.preventDefault()
		$(this).tab('show')
	});

	$('.dropdown-toggle').dropdown()
    
	@yield('js')
	</script>
</body>
</html>
