<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>

	<script src="{{ asset('public/js/jquery-3.1.0.min.js') }}"></script>
	<script src="{{ asset('public/js/mustache.min.js') }}"></script>
	<script src="{{url('/public')}}{{ mix('/js/app.js') }}"></script>
	<script src="{{url('/public')}}/js/datatables.min.js"></script>
    <script src="{{url('/public')}}{{ mix('/js/notify.js') }}"></script>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <link href="{{url('/public')}}{{ mix('/css/app.css') }}" rel="stylesheet">
    <link href="{{url('/public')}}/css/datatables.min.css" rel="stylesheet">
    <link href="{{url('/public')}}{{ mix('/css/notify.css') }}" rel="stylesheet">
	@yield('header')
</head>
<body>
    <div id="app">
        @include('nav.mainmenu')

		@include('common.errors')
        @yield('content')
    </div>

    <!-- Scripts -->
    <script lang="text/javascript">

    //Para mostrar tooltips con data-toggle
	$('body').tooltip({
	    selector: '[data-toggle="tooltip"]'
	});
	$('body').popover({
	    selector: '[data-toggle="popover"]'
	});

	//Activar Tabs. Tienen que tener la clase NavTabs
	$('.NavTabs a').click(function (e) {
		e.preventDefault()
		$(this).tab('show')
	});

	$('.dropdown-toggle').dropdown()
    
	@yield('js')
	</script>
</body>
</html>
