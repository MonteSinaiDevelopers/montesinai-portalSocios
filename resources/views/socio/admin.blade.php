@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Administrar Socios</div>

                <div class="panel-body">
                    <table class="SocioAdminTable table table-hover table-striped" id="SocioAdminTable">
                    	<thead>
                    		<tr>
	                    		<th>ID</th>
	                    		<th class="">Nombre</th>
	                    		<th class="datatables-filtercolumn-select">Sexo</th>
	                    		<th class="datatables-filtercolumn-select">Estado Civil</th>
	                    		<th class="datatables-filtercolumn-select">Comunidad</th>
	                    		<th>Cel.</th>
	                    		<th class="">E-Mail</th>
	                    		<th>FechaNac</th>
	                    		<th class="">Casa</th>
	                    		<th class="">&nbsp;</th>
	                    		<th>Tel.</th>
	                    	</tr>
                    	</thead>
                    	<tbody>
                    		<tr>
                    			<td colspan="12">Cargando tabla...</td>
                    		</tr>
                    	</tbody>
                    	<tfoot>
                    		<tr>
	                    		<th>ID</th>
	                    		<th>Nombre</th>
	                    		<th>Sexo</th>
	                    		<th>Estado Civil</th>
	                    		<th>Comunidad</th>
	                    		<th>Cel.</th>
	                    		<th>E-Mail</th>
	                    		<th>FechaNac</th>
	                    		<th>Casa</th>
	                    		<th>&nbsp;</th>
	                    		<th>Tel.</th>
	                    	</tr>
                    	</tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
$(document).ready(function(){
    $(window).on('beforeunload',function(){
      return '';
    });

	$.getJSON(
		"{{ route('socio/getListaJSON') }}",
		{
				
		},
		function(data){
			$.get({ url: "../resources/views/mustaches/socio/sociolistitem.html", cache: false, success: function(template){
					fnFecha = function(){
						return function(text, render){
							return formatDateSpanish(render(text));
						}
					}

					data.FechaFormat = fnFecha;
					data.CasaViewURL = "{{ route('casa/view') }}";
					data.ViewLabel = "{{ __('common.view_action') }}";
					data.CasaLabel = "{{ __('socio.casa') }}";

					$('#SocioAdminTable > tbody').mFill(data, {'template': template});

					if ( $.fn.dataTable.isDataTable( '#SocioAdminTable' ) ) {
						table = $('#SocioAdminTable').DataTable( { retrieve: true } );
    					table.destroy();
					}

					$('#SocioAdminTable').DataTable({
						"autoWidth": false,
						columnDefs: [
					        { "targets": 9, "searchable": false, "orderable": false }
					    ],
						initComplete: function () {
				            this.api().columns('.datatables-filtercolumn-select').every( function () {
				                var column = this;
				                var select = $('<select id="datatables-filtercolumn-select'+column.index()+'"><option value=""></option></select>')
				                    .appendTo( $(column.footer()).empty() )
				                    .on( 'change', function () {
				                        var val = $.fn.dataTable.util.escapeRegex(
				                            $(this).val()
				                        );
				 
				                        column
				                            .search( val ? '^'+val+'$' : '', true, false )
				                            .draw();
				                    } );
				 
				                column.data().unique().sort().each( function ( d, j ) {
				                    select.append( '<option value="'+d+'">'+d+'</option>' )
				                } );
				            } );
						}
					});
				}
			});
		},
	);
});
@endsection