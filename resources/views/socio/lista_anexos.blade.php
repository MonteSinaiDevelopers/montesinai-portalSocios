@extends('layouts.app')

@section('title')
Estado de Cuenta
@endsection

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="col-sm-8 col-sm-offset-2">
    	<h1>Lista Anexos de Ventas - Socio #<span id="IDSocio">&nbsp;</span></h1>

		<table id="ListaAnexosTable" class="ListaAnexosTable table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>Nombre de Archivo</th>
					<th>Fecha</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="2">Cargando tabla...</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</tfoot>
		</table>
    </div>
@endsection

@section('js')

$(document).ready(function(){
	$.getJSON(
		"{{ url('/socio/getAnexosAsJSON') }}",
		{
		Acceso: "{{ session('acceso', 0) }}"		
		},
		function(data){
			$('#ListaAnexosTable').fillTable(data.Anexos, {
				columns: ['actions:link', '@Alta'],
				linkaction: {
					link: '{{route('anexo/download')}}?IDR=%%@IDR%%&Acceso={{ session('acceso', 0) }}',
					label: '%%@Nombre%%'
				}
			});
		}
	);
});
@endsection