@extends('layouts.app')

@section('title')
Estado de Cuenta
@endsection

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="col-sm-8 col-sm-offset-2">
    	<h1>Saldos Pendientes - Socio #<span id="IDSocio">&nbsp;</span></h1>

		<table id="CxcPendientesSocioTable" class="CxcPendientesSocio table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>Cliente</th>
					<th>Nombre</th>
					<th>ID Movimiento</th>
					<th>Fecha</th>
					<th>Saldo</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="5">Cargando tabla...</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5">&nbsp;</td>
				</tr>
			</tfoot>
		</table>
    </div>
@endsection

@section('js')

$(document).ready(function(){
	// id_socio o acceso

	$.getJSON(
		"{{ url('/socio/getCxCPendientesJSON') }}",
		{
		id_socio: "{{ request('id_socio', 0) }}"		
		},
		function(data){
			$('#CxcPendientesSocioTable > tbody').empty();

			data.FechaEmision_format = function() {
				var d = new Date(this['@FechaEmision']);
				return formatDateSpanish(d);
			}
			data.Saldo_format = function() {
				if(!isNaN(this['@Saldo'])){
					return '$ ' + formatMoney(this['@Saldo'].toFixed(2));
				}
				else {
					return '';
				}
			}

			data.Sum_Saldo = 0;
			for(var row in data.CxCPendientes){
				if(!isNaN(data.CxCPendientes[row]['@Saldo'])){
					data.Sum_Saldo += data.CxCPendientes[row]['@Saldo'];
				}
			}

			data.Sum_Saldo_format = function() {
				return '$ ' + formatMoney(this['Sum_Saldo'].toFixed(2));
			}

			$('#CxcPendientesSocioTable > tbody').append(Mustache.render('@{{#CxCPendientes}}'+
																		'<tr>'+
																		'	<td>@{{{@Cliente}}}</td>'+
																		'	<td>@{{{@Nombre}}}</td>'+
																		'	<td>@{{{@Mov}}} @{{{@MovID}}}</td>'+
																		'	<td>@{{FechaEmision_format}}</td>'+
																		'	<td>@{{Saldo_format}}</td>'+
																		'</tr>'+
																		'@{{/CxCPendientes}}'+
																		'@{{^CxCPendientes}}<tr><td>@{{msg_no_data}}</td></tr>@{{/CxCPendientes}}', data));

			$('#CxcPendientesSocioTable > tfoot').empty();
			$('#CxcPendientesSocioTable > tfoot').append(Mustache.render('<tr>'+
																			'<td colspan=4 class="text-right">Total: </td>'+
																			'<td>@{{Sum_Saldo_format}}</td>'+
																		'</tr>', data));
		}
	);
});
@endsection