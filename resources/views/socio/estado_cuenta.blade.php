@extends('layouts.app')

@section('title')
Estado de Cuenta
@endsection

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="col-sm-8 col-sm-offset-2">
    	<h1>Estado de Cuenta - Socio #<span id="IDSocio">&nbsp;</span></h1>

		<table id="EstadoCuentaSocioTable" class="EstadoCuentaSocio table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>Cliente</th>
					<th>Nombre</th>
					<th>ID Movimiento</th>
					<th>Fecha</th>
					<th>Abono</th>
					<th>Cargo</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="5">Cargando tabla...</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5">&nbsp;</td>
				</tr>
			</tfoot>
		</table>
    </div>
@endsection

@section('js')

$(document).ready(function(){
	$.getJSON(
		"{{ url('/socio/getEstadoCuentaJSON') }}",
		{
		id_socio: "{{ request('id_socio', 0) }}"		
		},
		function(data){
			$('#EstadoCuentaSocioTable > tbody').empty();

			data.Fecha_format = function() {
				var d = new Date(this['@Fecha']);
				return formatDateSpanish(d);
			}
			data.Abono_format = function() {
				if(!isNaN(this['@Abono'])){
					return '$ ' + formatMoney(this['@Abono'].toFixed(2));
				}
				else {
					return '';
				}
			}
			data.Cargo_format = function() {
				if(!isNaN(this['@Cargo'])){
					return '$ ' + formatMoney(this['@Cargo'].toFixed(2));
				}
				else {
					return '';
				}
			}

			data.Sum_Abonos = 0; data.Sum_Cargos = 0;
			for(var row in data.EstadoCuentaDetalle){
				if(!isNaN(data.EstadoCuentaDetalle[row]['@Abono'])){
					data.Sum_Abonos += data.EstadoCuentaDetalle[row]['@Abono'];
				}

				if(!isNaN(data.EstadoCuentaDetalle[row]['@Cargo'])){
					data.Sum_Cargos += data.EstadoCuentaDetalle[row]['@Cargo'];
				}
			}
			data.Sum_Abonos_format = function() {
				return '$ ' + formatMoney(this['Sum_Abonos'].toFixed(2));
			}
			data.Sum_Cargos_format = function() {
				return '$ ' + formatMoney(this['Sum_Cargos'].toFixed(2));
			}

			$('#EstadoCuentaSocioTable > tbody').append(Mustache.render('@{{#EstadoCuentaDetalle}}'+
																		'<tr>'+
																		'	<td>@{{{@Cuenta}}}</td>'+
																		'	<td>@{{{@Nombre}}}</td>'+
																		'	<td>@{{{@Mov}}} @{{{@MovID}}}</td>'+
																		'	<td>@{{Fecha_format}}</td>'+
																		'	<td>@{{Abono_format}}</td>'+
																		'	<td>@{{Cargo_format}}</td>'+
																		'</tr>'+
																		'@{{/EstadoCuentaDetalle}}'+
																		'@{{^EstadoCuentaDetalle}}<tr><td>@{{msg_no_data}}</td></tr>@{{/EstadoCuentaDetalle}}', data));

			$('#EstadoCuentaSocioTable > tfoot').empty();
			$('#EstadoCuentaSocioTable > tfoot').append(Mustache.render('<tr>'+
																			'<td colspan=4 class="text-right">Total: </td>'+
																			'<td>@{{Sum_Abonos_format}}</td>'+
																			'<td>@{{Sum_Cargos_format}}</td>'+
																		'</tr>', data));
		}
	);
});
@endsection