@extends('layouts.app')

@section('content')
<div class="container">
	<form id="BMCreateForm" method="POST" action="{{ route('barmitzvah/new_submit') }}" enctype="multipart/form-data">
	{{ csrf_field() }}
		<input type="hidden" name="BMCreateNinoID_Socio" id="BMCreateNinoID_Socio" value="{{ $datos_kid['@ID_Socio'] }}" />
		<div class="col-sm-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Datos Niño</div>

				<div class="panel-body">
					<fieldset>
						<div class="form-group row {{$form_errors['BMCreateNinoNombreHasError'] or ''}}">
							<label for="BMCreateNinoNombre" class="col-form-label col-sm-2">Nombre(s)</label>
							<div class="col-sm-9 col-sm-offset-1">
								<input type="text" size="50" class="form-control" id="BMCreateNinoNombre" name="BMCreateNinoNombre" value="{{ request('BMCreateNinoNombre', $datos_kid['@Nombre']) }}" />
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreateNinoApellidoPaternoHasError'] or ''}}">
							<label for="BMCreateNinoApellidoPaterno" class="col-form-label col-sm-2">Apellido Paterno</label>
							<div class="col-sm-9 col-sm-offset-1">
								<input type="text" size="50" class="form-control" id="BMCreateNinoApellidoPaterno" name="BMCreateNinoApellidoPaterno" value="{{ request('BMCreateNinoApellidoPaterno', $datos_kid['@APaterno']) }}" />
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreateNinoApellidoMaternoHasError'] or ''}}">
							<label for="BMCreateNinoApellidoMaterno" class="col-form-label col-sm-2">Apellido Materno</label>
							<div class="col-sm-9 col-sm-offset-1">
								<input type="text" size="50" class="form-control" id="BMCreateNinoApellidoMaterno" name="BMCreateNinoApellidoMaterno" value="{{ request('BMCreateNinoApellidoMaterno', $datos_kid['@AMaterno']) }}" />
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreateNinoFechaNacHasError'] or ''}}">
							<label for="BMCreateNinoFechaNac" class="col-form-label col-sm-2">Fecha de Nacimiento</label>
							<div class="col-sm-2 col-sm-offset-1">
								<input type="date" class="form-control {{$form_errors['BMCreateNinoFechaNacHasError'] or ''}}" id="BMCreateNinoFechaNac" name="BMCreateNinoFechaNac" value="{{ request('BMCreateNinoFechaNac', $datos_kid['@FechaNacimiento']) }}" />
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreateNinoFechaNacHasError'] or ''}}">
							<div class="col-sm-4 col-sm-offset-3">
								Fecha hebrea de nacimiento: {{ $datos_kid['@FechaNacimientoHebreo']}}
							</div>
						</div>
						<div class="form-group row {{$form_errors['BMCreateNinoFechaNacHasError'] or ''}}">
							<div class="col-sm-4 col-sm-offset-3">
								Fecha hebrea de Bar-Mitzvah: {{ $datos_kid['@FechaBMHebreo']}}
							</div>
						</div>
						<div class="form-group row {{$form_errors['BMCreateNinoFechaNacHasError'] or ''}}">
							<div class="col-sm-4 col-sm-offset-3">
								Fecha común de Bar-Mitzvah: {{ $datos_kid['@FechaBMComun']}}
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreateNinoActaHasError'] or ''}}">
							<label for="BMCreateNinoActa" class="col-form-label col-sm-2">Acta de Nacimiento</label>
							<div class="col-sm-4 col-sm-offset-1">
								<input type="file" class="form-control {{$form_errors['BMCreateNinoFechaNacHasError'] or ''}}" name="BMCreateNinoActa" id="BMCreateNinoActa" />
							</div>
						</div>						

						<div class="form-group row {{$form_errors['BMCreateNinoEscolaridadPrimariaHasError'] or ''}}">
							<label for="BMCreateNinoEscolaridadPrimaria" class="col-form-label col-sm-2">Escuela Primaria (cursada o cursando)</label>
							<div class="col-sm-9 col-sm-offset-1">
								<input type="text" size="50" class="form-control" id="BMCreateNinoEscolaridadPrimaria" name="BMCreateNinoEscolaridadPrimaria" value="{{ request('BMCreateNinoEscolaridadPrimaria', '') }}" />
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreateNinoEscolaridadSecundariaHasError'] or ''}}">
							<label for="BMCreateNinoEscolaridadSecundaria" class="col-form-label col-sm-2">Escuela Secundaria (cursada o cursando)</label>
							<div class="col-sm-9 col-sm-offset-1">
								<input type="text" size="50" class="form-control" id="BMCreateNinoEscolaridadSecundaria" name="BMCreateNinoEscolaridadSecundaria" value="{{ request('BMCreateNinoEscolaridadSecundaria', '') }}" />
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreateNinoCLIHasError'] or ''}}">
							<div class="col-sm-4 col-sm-offset-3">
								Elija si el niño es:
								<div class="radio">
									<label for="BMCreateNinoCLI-cohen">
										<input type="radio" id="BMCreateNinoCLI-cohen" name="BMCreateNinoCLI" value="cohen" />Cohen
									</label>
								</div>
								<div class="radio">
									<label for="BMCreateNinoCLI-levy">
										<input type="radio" id="BMCreateNinoCLI-levy" name="BMCreateNinoCLI" value="levy" />Levy
									</label>
								</div>
								<div class="radio">
									<label for="BMCreateNinoCLI-israel">
										<input type="radio" id="BMCreateNinoCLI-israel" name="BMCreateNinoCLI" value="israel" />Israel
									</label>
								</div>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="panel panel-primary">
				<div class="panel-heading">Datos Padre (O Tutor)</div>

				<div class="panel-body">
					<fieldset>
						<div class="form-group row {{$form_errors['BMCreatePadreNombreHasError'] or ''}}">
							<label for="BMCreatePadreNombre" class="col-form-label col-sm-2">Nombre(s)</label>
							<div class="col-sm-9 col-sm-offset-1">
								<input type="text" size="50" class="form-control" id="BMCreatePadreNombre" name="BMCreatePadreNombre" value="{{ request('BMCreatePadreNombre', $datos_padre['BMPadreNombre']) }}" />
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreatePadreApellidoPaternoHasError'] or ''}}">
							<label for="BMCreatePadreApellidoPaterno" class="col-form-label col-sm-2">Apellido Paterno</label>
							<div class="col-sm-9 col-sm-offset-1">
								<input type="text" size="50" class="form-control" id="BMCreatePadreApellidoPaterno" name="BMCreatePadreApellidoPaterno" value="{{ request('BMCreatePadreApellidoPaterno', $datos_padre['BMPadreApellidoPaterno']) }}" />
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreatePadreApellidoMaternoHasError'] or ''}}">
							<label for="BMCreatePadreApellidoMaterno" class="col-form-label col-sm-2">Apellido Materno</label>
							<div class="col-sm-9 col-sm-offset-1">
								<input type="text" size="50" class="form-control" id="BMCreatePadreApellidoMaterno" name="BMCreatePadreApellidoMaterno" value="{{ request('BMCreatePadreApellidoMaterno', $datos_padre['BMPadreApellidoMaterno']) }}" />
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreatePadreMailHasError'] or ''}}">
							<label for="BMCreatePadreMail" class="col-form-label col-sm-2">eMail</label>
							<div class="col-sm-9 col-sm-offset-1">
								<input type="text" size="50" class="form-control" id="BMCreatePadreMail" name="BMCreatePadreMail" value="{{ request('BMCreatePadreMail', $datos_padre['BMPadreMail']) }}" />
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreatePadreTelefonoHasError'] or ''}}">
							<label for="BMCreatePadreTelefono" class="col-form-label col-sm-2">Telefono Personal</label>
							<div class="col-sm-9 col-sm-offset-1">
								<input type="text" size="50" class="form-control" id="BMCreatePadreTelefono" name="BMCreatePadreTelefono" value="{{ request('BMCreatePadreTelefono', $datos_padre['BMPadreTelefono']) }}" />
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
		
		<div class="col-sm-6">
			<div class="panel panel-primary">
				<div class="panel-heading">Datos Madre (O Tutora)</div>

				<div class="panel-body">
					<fieldset>
						<div class="form-group row {{$form_errors['BMCreateMadreNombreHasError'] or ''}}">
							<label for="BMCreateMadreNombre" class="col-form-label col-sm-2">Nombre(s)</label>
							<div class="col-sm-9 col-sm-offset-1">
								<input type="text" size="50" class="form-control" id="BMCreateMadreNombre" name="BMCreateMadreNombre" value="{{ request('BMCreateMadreNombre', $datos_madre['BMMadreNombre']) }}" />
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreateMadreApellidoPaternoHasError'] or ''}}">
							<label for="BMCreateMadreApellidoPaterno" class="col-form-label col-sm-2">Apellido Paterno</label>
							<div class="col-sm-9 col-sm-offset-1">
								<input type="text" size="50" class="form-control" id="BMCreateMadreApellidoPaterno" name="BMCreateMadreApellidoPaterno" value="{{ request('BMCreateMadreApellidoPaterno', $datos_madre['BMMadreApellidoPaterno']) }}" />
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreateMadreApellidoMaternoHasError'] or ''}}">
							<label for="BMCreateMadreApellidoMaterno" class="col-form-label col-sm-2">Apellido Materno</label>
							<div class="col-sm-9 col-sm-offset-1">
								<input type="text" size="50" class="form-control" id="BMCreateMadreApellidoMaterno" name="BMCreateMadreApellidoMaterno" value="{{ request('BMCreateMadreApellidoMaterno', $datos_madre['BMMadreApellidoMaterno']) }}" />
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreateMadreMailHasError'] or ''}}">
							<label for="BMCreateMadreMail" class="col-form-label col-sm-2">eMail</label>
							<div class="col-sm-9 col-sm-offset-1">
								<input type="text" size="50" class="form-control" id="BMCreateMadreMail" name="BMCreateMadreMail" value="{{ request('BMCreateMadreMail', $datos_madre['BMMadreMail']) }}" />
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreateMadreTelefonoHasError'] or ''}}">
							<label for="BMCreateMadreTelefono" class="col-form-label col-sm-2">Telefono Personal</label>
							<div class="col-sm-9 col-sm-offset-1">
								<input type="text" size="50" class="form-control" id="BMCreateMadreTelefono" name="BMCreateMadreTelefono" value="{{ request('BMCreateMadreTelefono', $datos_madre['BMMadreTelefono']) }}" />
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>

		<div class="col-sm-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Datos Evento Bar-Mitzvah</div>

				<div class="panel-body">
					<h2>Estas opciones son simplemente sugerencias para motivos de prevención y estadística. No es una reserva final ni se puede garantizar que el evento se pueda llevar a cabo en los templos elegidos. La reserva final se hará presencialmente en las instalaciones de Monte Sinai en una fecha futura y con un procedimiento a comunicarse posteriormente.</h2>
					<fieldset>
						<div class="form-group row {{$form_errors['BMCreateTemploOpcion1HasError'] or ''}}">
							<label for="BMCreateTemploOpcion1" class="col-form-label col-sm-2 col-sm-offset-3">Opcion Templo 1</label>
							<div class="col-sm-4">
								<select name="BMCreateTemploOpcion1" id="BMCreateTemploOpcion1" class="form-control">
									<option value="">Elija uno</option>
									<option value="01">Beth Moshe</option>
									<option value="02">Beth Yosef</option>
									<option value="03">Queretaro</option>
									<option value="12">Shaar LeSimja</option>
								</select>
							</div>
						</div>

						<div class="form-group row {{$form_errors['BMCreateTemploOpcion2HasError'] or ''}}">
							<label for="BMCreateTemploOpcion2" class="col-form-label col-sm-2 col-sm-offset-3">Opcion Templo 2</label>
							<div class="col-sm-4">
								<select name="BMCreateTemploOpcion2" id="BMCreateTemploOpcion2" class="form-control">
									<option value="">Elija uno</option>
									<option value="01">Beth Moshe</option>
									<option value="02">Beth Yosef</option>
									<option value="03">Queretaro</option>
									<option value="12">Shaar LeSimja</option>
								</select>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>

		<div class="col-sm-12">
			<div class="panel panel-primary">
				<div class="panel-body">
					<input type="submit" class="form-control btn btn-primary" id="BMCreateSubmit" name="BMCreateSubmit" value="Enviar" />
				</div>
			</div>
		</div>
	</form>
</div>
@endsection

@section('js')
$('#BMCreateTemploOpcion1').change(function(){
	$.each($('#BMCreateTemploOpcion2').children(), function(index, value){
		$(value).removeAttr("disabled");
	});

	var selected_value = $('#BMCreateTemploOpcion1').children("option:selected").val();

	$('#BMCreateTemploOpcion2 option[value="'+selected_value+'"]').attr('disabled', true);
});
@endsection