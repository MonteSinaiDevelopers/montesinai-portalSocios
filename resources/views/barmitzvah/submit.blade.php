@extends('layouts.app')

@section('content')
<div class="container">
	<div class="panel panel-primary">
		<div class="panel-body">Se recibieron los datos ingresados. En una fecha posterior se enviará a su mismo correo las instrucciones con la siguiente fase de la inscripción</div>
	</div>
</div>
@endsection

@section('js')

@endsection