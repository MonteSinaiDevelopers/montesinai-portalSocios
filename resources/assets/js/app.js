
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//require('./bootstrap');

//window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example', require('./components/Example.vue'));

/*
const app = new Vue({
    el: '#app'
});
*/

var getScriptLocation = function() {
	var fileName  	= "fileName";
	var stack       = "stack";
	var stackTrace  = "stacktrace";
	var loc   	= null;
 
	var matcher = function(stack, matchedLoc) { return loc = matchedLoc; };
 
	try { 
 
		// Invalid code
		0();
 
	}  catch (ex) {
 
		if(fileName in ex) { // Firefox
			loc = ex[fileName];
		} else if(stackTrace in ex) { // Opera
			ex[stackTrace].replace(/called from line \d+, column \d+ in (.*):/gm, matcher);
		} else if(stack in ex) { // WebKit, Blink and IE10
			ex[stack].replace(/at.*?\(?(\S+):\d+:\d+\)?$/g, matcher);
		}

		if(loc === null){
            return '';
        }
 
		return loc;
	}
 
};

var loc = getScriptLocation().replace("/public/js/app.js",  ""); //Este archivo esta en resources/assets/js, pero al mixearlo termina en public

function doNotify(Vmsg, Vtype = "success", Vtimeout = 5000, Vpos = "top-center", VzIndex = 10400){
	if(Vmsg.trim() == ""){
		Vmsg = 'Error Generico';
	}
	
	$.notify({
		message: Vmsg,
		status: Vtype,
		timeout: Vtimeout,
		pos: Vpos,
		zIndex: VzIndex
	});
}

window.doNotify = doNotify;

$(document).ready(function(){
	$.fn.extend({
		fillTable: function(data, options = {}) {
			table = $(this);
			table.children('tbody').empty();

			if(data == undefined || data.length == 0){
				var colspan = table.children('th').length;

				if('msg_no_data' in options){
					table.children('tbody').append('<tr><td colspan="'+colspan+'">'+options.msg_no_data+'</td></tr>');
				}
				else
				{
					table.children('tbody').append('<tr><td colspan="'+colspan+'">No hay datos</td></tr>');
				}
				return
			}

			$.each(data, function(index, row){
				thisrow = '<tr>';
				if(!('columns' in options) || options.columns.length == 0){
					$.each(row, function(key, value){
						thisrow += '<td>'+value+'</td>';
					});
				}
				else {
					$.each(options.columns, function(index, columnname){
						if(columnname in row){
							thisrow += '<td>'+row[columnname]+'</td>';
						}
						else {
							if(columnname.indexOf(':') > 0){
								var actionrow = [];
								if(columnname.startsWith('actions:')){
									$.each(columnname.replace('actions:', '').split(','), function(index, action){
										if(action+'action' in options){
											var link = options[action+'action'].link;
											var label = options[action+'action'].label;

											$.each(row, function(index, field){
												link = link.replace('%%'+index+'%%', field);
												label = label.replace('%%'+index+'%%', field);										
											});
											actionrow.push('<a href="'+link+'">'+label+'</a>');
										}
										else {
											actionrow.push('<a href="#">Action</a>');
										}
									});
								}
								thisrow += '<td>'+actionrow.join('|')+'</td>';
							}
							else {
								thisrow += '<td>N/A</td>';
							}
						}
					});
				}
				thisrow += '</tr>';
				table.children('tbody').append(thisrow);
			});
		},
		fillList: function(data, options = {}) {
			list = $(this);
			list.empty();

			if(data == undefined || data.length == 0){
				if('msg_no_data' in options){
					list.append('<li class="list-group-item">'+options.msg_no_data+'</li>');
				}
				else
				{
					list.append('<li class="list-group-item">No hay datos</li>');
				}
				return
			}

			$.each(data, function(index, row){
				thisrow = '<li class="list-group-item">';
				if(!('columns' in options) || options.columns.length == 0){
					$.each(row, function(key, value){
						thisrow += value;
					});
				}
				else {
					$.each(options.columns, function(index, columnname){
						if(columnname in row){
							thisrow += row[columnname];
						}
						else {
							if(columnname.indexOf(':') > 0){
								var actionrow = [];
								if(columnname.startsWith('actions:')){
									$.each(columnname.replace('actions:', '').split(','), function(index, action){
										if(action+'action' in options){
											var link = options[action+'action'].link;
											var label = options[action+'action'].label;

											$.each(row, function(index, field){
												link = link.replace('%%'+index+'%%', field);
												label = label.replace('%%'+index+'%%', field);										
											});
											actionrow.push('<a href="'+link+'">'+label+'</a>');
										}
										else {
											actionrow.push('<a href="#">Action</a>');
										}
									});
								}
								thisrow += actionrow.join('|');
							}
							else {
								thisrow += 'N/A';
							}
						}
					});
				}
				thisrow += '</li>';
				list.append(thisrow);
			});
		},
		fillSelect: function(data, options = {}) {
			list = $(this);
			list.empty();

			if(data == undefined || data.length == 0){
				if('msg_no_data' in options){
					list.append('<option value="">'+options.msg_no_data+'</option>');
				}
				else
				{
					list.append('<option value="">No hay datos</option>');
				}
				return
			}

			$.each(data, function(index, row){
				if(row[options['value_colname']] == options['selected_value']){
					thisrow = '<option value="'+row[options['value_colname']]+'" selected="selected">'+row[options['label_colname']]+'</option>';
				}
				else {
					thisrow = '<option value="'+row[options['value_colname']]+'">'+row[options['label_colname']]+'</option>';
				}
				list.append(thisrow);
			});
		},
		mFill: function(data, options = {}) {
			var parent = $(this);
			parent.empty();
			var parentid = parent[0].id;

			if(('templatename' in options) != true && ('template' in options) != true){
				parent.append('No template defined');
				return;
			}

			booleanClassFunction = function(){
				return function(text){
					if(text in this && (this[text] == true || this[text] == 1)){
						return "success";
					}
					else {
						return "danger";
					}
				};
			};

			getCheckedStatusFunction = function(){
				return function(text){
					if(text in this && (this[text] == true || this[text] == 1)){
						return 'checked="checked"';
					}
					else {
						return '';
					}
				}
			};

			var dataobject = $.extend({parentid: parentid}, data, {getBooleanClass: booleanClassFunction, appPath: loc, getCheckedStatus: getCheckedStatusFunction});

			if('templatename' in options && options.templatename.trim() != ''){
				$.get({ url: "../resources/views/mustaches/"+options.templatename+".html", cache: false, success: function(template){
					template = Mustache.render(template, dataobject);
					parent.append(template);
					}
				});
			}

			if('template' in options && options.template.trim() != ''){
				template = Mustache.render(options.template, dataobject);
				parent.append(template);
			}
		},
		doJsonAction: function(params = {}, successFunction = function(data, object){}){
			if($(this).attr("jsonaction") == undefined){
				return;
			}
			
			var objectRef = $(this);
			
			var action = $(this).attr("jsonaction");
			$.getJSON(
				action,
				params,
				function(data, VtextStatus, VjqXHR){
					if(('success' in data) == false && (('msg' in data) == false || data.msg.trim() == "")){
						//Todo: Mostrar error generico
						doNotify("Error JSON generico", "error");
						return;
					}
					window[successFunction](data, objectRef);
					if(data.success == 1){
						doNotify(data.msg, 'success');
						return;
					}
					else {
						doNotify(data.msg, 'error');
						return;
					}
				}
			);
		}
	});
});

$(document).ready(function(){
	$('body').on('click', 'a.jsonaction-link', function(event){
		$(this).doJsonAction($(this).attr("jsonaction-params"), $(this).attr("jsonaction-onsuccess"));
	});

	$('body').on('change', 'select.jsonaction-select', function(event){
		var params = {}
		params['val'] = $(this).val();
		$(this).doJsonAction(params, $(this).attr("jsonaction-onsuccess"));
	});
});

window.formatMoney = function(n, c = 2, d = '.', t = ','){
	var c = isNaN(c = Math.abs(c)) ? 2 : c, 
	    d = d == undefined ? "." : d, 
	    t = t == undefined ? "," : t, 
	    s = n < 0 ? "-" : "", 
	    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
	    j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	 };

window.monthsTextSpanish = [];
window.monthsTextSpanish[0] = 'Enero';
window.monthsTextSpanish[1] = 'Febrero';
window.monthsTextSpanish[2] = 'Marzo';
window.monthsTextSpanish[3] ='Abril';
window.monthsTextSpanish[4] = 'Mayo';
window.monthsTextSpanish[5] = 'Junio';
window.monthsTextSpanish[6] = 'Julio';
window.monthsTextSpanish[7] = 'Agosto';
window.monthsTextSpanish[8] = 'Septiembre';
window.monthsTextSpanish[9] = 'Octubre';
window.monthsTextSpanish[10] = 'Noviembre';
window.monthsTextSpanish[11] = 'Diciembre';

window.formatDateSpanish = function(date){
	if(typeof date == 'string'){
		var newdate = new Date(date);
		return newdate.getDate()+'/'+window.monthsTextSpanish[newdate.getMonth()]+'/'+newdate.getFullYear();
	}
	if(date instanceof Date){
		return date.getDate()+'/'+window.monthsTextSpanish[date.getMonth()]+'/'+date.getFullYear();

	}

	return date;
};