<?php

return [
	'default_action' => 'Action',
	'edit_action' => 'Edit',
	'delete_action' => 'Delete',
	'view_action' => 'View',
	'empty' => 'Empty'
	'all' => 'All',
];
