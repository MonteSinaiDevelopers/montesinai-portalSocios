<?php

return [
	'default_action' => 'Accion',
	'edit_action' => 'Editar',
	'delete_action' => 'Eliminar',
	'view_action' => 'Ver',
	'empty' => 'Vacio',
	'all' => 'Todos/as',
	'all-m' => 'Todos',
	'all-f' => 'Todas',
];
