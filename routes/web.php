<?php

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();
Route::get('/acceso', 'AccesoController@main')->name('acceso');

Route::get('/user/admin', function(){
	return view('user.admin');
})->middleware(['permission:read-users'])->name('user/admin');
Route::get('/user/get', 'UserController@get')->middleware(['permission:read-users'])->name('user/get');
Route::get('/user/edit', 'UserController@edit')->middleware(['permission:update-users'])->name('user/edit');
Route::get('/user/getIntelisisUsersJSON', 'UserController@getIntelisisUsersJSON')->middleware(['permission:update-users'])->name('user/getIntelisisUsersJSON');
Route::post('/user/edit', 'UserController@modify')->middleware(['permission:update-users'])->name('user/modify');
Route::post('/user/edit_perms', 'UserController@modify_perms')->middleware(['permission:update-users'])->name('user/edit_perms');
Route::get('/user/delete', 'UserController@delete')->middleware(['permission:delete-users'])->name('user/delete');
Route::get('/profile/edit', 'UserController@editProfile')->middleware(['role:user'])->name('profile/edit');
Route::post('/profile/modify', 'UserController@modifyProfile')->middleware(['role:user'])->name('profile/modify');

Route::get('/socio/getCxCPendientes', 'EstadoCuentaController@getCxCPendientes')->middleware(['permission:read-socioscxc'])->name('socio/getCxCPendientes');
Route::get('/socio/getEstadoCuenta', 'EstadoCuentaController@getFromGet')->middleware(['permission:read-socioscxc'])->name('socio/getEstadoCuenta');
Route::get('/socio/getCxCPendientesJSON', 'EstadoCuentaController@getCxCPendientesJSON')->middleware(['permission:read-socioscxc'])->name('socio/getCxCPendientesJSON');
Route::get('/socio/getEstadoCuentaJSON', 'EstadoCuentaController@getAsJSON')->middleware(['permission:read-socioscxc']);
Route::get('/socio/getCxCPendientesAcceso', 'EstadoCuentaController@getCxCPendientesAcceso')->name('socio/getCxCPendientesAcceso');
Route::get('/socio/getEstadoCuentaAcceso', 'EstadoCuentaController@getEstadoCuentaAcceso')->name('socio/getEstadoCuentaAcceso');
Route::get('/socio/getCxCPendientesAccesoJSON', 'EstadoCuentaController@getCxCPendientesAccesoJSON')->name('socio/getCxCPendientesAccesoJSON');
Route::get('/socio/getEstadoCuentaAccesoJSON', 'EstadoCuentaController@getEstadoCuentaAccesoJSON');

Route::get('/socio/anexo', 'SocioController@getListaAnexos')->name('socio/anexo');
Route::get('/socio/getAnexosAsJSON', 'SocioController@getAnexosAsJSON')->name('socio/getAnexosAsJSON');
Route::get('/anexo/download', 'AnexoController@download')->name('anexo/download');
Route::get('/socio/admin', 'SocioController@admin')->middleware(['permission:read-socios'])->name('socio/admin');
Route::get('/socio/getListaJSON', 'SocioController@getListaJSON')->middleware(['permission:read-socios'])->name('socio/getListaJSON');

Route::get('/casa', 'CasaController@view')->middleware(['permission:read-socios'])->name('casa/view');

Route::get('/caso/admin', 'CasoController@admin')->middleware(['permission:read-caso'])->name('caso/admin');
Route::get('/caso/get', 'CasoController@get')->middleware(['permission:read-caso'])->name('caso/get');
Route::get('/caso/getJSON', 'CasoController@getJSON')->middleware(['permission:read-caso'])->name('caso/getJSON');
Route::get('/caso/new', 'CasoController@create')->name('caso/new');
Route::post('/caso/new', 'CasoController@create')->name('caso/new_submit');
Route::get('/caso/deleteJSON', 'CasoController@deleteJSON')->name('caso/deleteJSON');
Route::get('/caso/asignarJSON', 'CasoController@asignarJSON')->name('caso/asignarJSON');
Route::get('/caso/actualizarJSON', 'CasoController@actualizarJSON')->name('caso/actualizarJSON');
Route::get('/caso/getAnexosJSON', 'CasoController@getAnexosJSON')->name('caso/getAnexosJSON');
Route::get('/caso/getAnexo', 'CasoController@getAnexo')->name('caso/getAnexo');

Route::get('/casoevento/getJSON', 'CasoEventoController@getJSON')->middleware(['permission:read-caso'])->name('casoevento/getJSON');
Route::get('/casoevento/editJSON', 'CasoEventoController@editJSON')->middleware(['permission:update-caso'])->name('casoevento/editJSON');
Route::get('/casoevento/deleteJSON', 'CasoEventoController@deleteJSON')->middleware(['permission:delete-caso'])->name('casoevento/deleteJSON');
Route::get('/casoevento/createJSON', 'CasoEventoController@createJSON')->middleware(['permission:create-caso'])->name('casoevento/createJSON');

Route::get('/barmitzvah/new/', 'BMController@create')->name('barmitzvah/new');
Route::post('/barmitzvah/new_submit', 'BMController@create_submit')->name('barmitzvah/new_submit');

// Route::get('runartisan', function () {
// 	if(!isset($_GET["args"])){
// 		$_GET["args"] = array();
// 	}
	
// 	set_time_limit(1000000);
// 	$artisan = Artisan::call($_GET["command"], $_GET["args"]);
// 	dd(Artisan::output());
// } );