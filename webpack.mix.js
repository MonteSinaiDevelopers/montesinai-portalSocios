let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/app.scss', 'public/css/app.css').options({processCssUrls: false});

mix.js('resources/assets/js/app.js', 'public/js/app.js')
	.js('resources/assets/js/notify.js', 'public/js/notify.js')
	.styles('resources/assets/css/notify.css', 'public/css/notify.css')
	.styles('resources/assets/css/home.css', 'public/css/home.css');

if (mix.inProduction()) {
    mix.version();
}