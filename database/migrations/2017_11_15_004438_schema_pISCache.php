<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchemaPISCache extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    	Schema::create('pis_cache', function (Blueprint $table) {
    		$table->increments("id")->identity();
    		$table->string("usuario_intelisis");
    		$table->string("referencia");
    		$table->text("params")->nullable();
    		$table->longText("resultado")->nullable();
    		$table->timestamps();
    	});  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    	Schema::dropIfExists('pis_cache');
    }
}
