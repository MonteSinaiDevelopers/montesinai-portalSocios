<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchemaCasos extends Migration
{
	private $to_create = [
			"directivo-casos" => [
					"display_name" => "Directivo Ahavat Shalom",
					"permissions" => [
							"read-casos" => "Ver Casos",
							"crear-casos" => "Crear Casos",
							"update-casos" => "Editar Casos",
							"delete-casos" => "Eliminar Casos",
							"eventos-casos" => "Agregar Eventos a Casos",
					]
			]
	];
	
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    	foreach($this->to_create as $rolename => $roledetails){
    		print("Creating '{$rolename}' role".PHP_EOL);
    		$role = App\Role::create(['name' => $rolename, 'display_name' => $roledetails['display_name'], 'description' => $roledetails['display_name'], ]);
    		
    		foreach($roledetails['permissions'] as $perm => $permdetails){
    			print("Creating '{$perm}' permission".PHP_EOL);
    			$permission = App\Permission::create(['name' => $perm, 'display_name' => $permdetails, 'description' => $permdetails,]);
    			print("Attaching '{$permission}' to '{$rolename}'".PHP_EOL);
    			$role->attachPermission($permission);
    		}
    	}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    	foreach($this->to_create as $role => $roledetails){   		
    		foreach($role['permissions'] as $perm => $permdetails){
    			print("Detaching '{$permission}' from '{$role}'");
    			$role->detachPermission($perm);
    			print("Deleting '{$perm}' permission");
    			$permission = App\Permission::destroy([$perm->id]);
    		}
    		
    		print("Deleting '{$role}' role");
    		$role = App\Role::destroy([$role->id]);
    	}
    }
}
