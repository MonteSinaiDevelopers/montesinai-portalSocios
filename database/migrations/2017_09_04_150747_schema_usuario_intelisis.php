<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchemaUsuarioIntelisis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    	Schema::table('users', function (Blueprint $table) {
    		$table->string("usuario_intelisis")->nullable();
    		$table->string("password_intelisis")->nullable();
    	}); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    	Schema::table('users', function ($table) {
    		$table->dropColumn('usuario_intelisis');
    		$table->dropColumn('password_intelisis');
    	});
    }
}
