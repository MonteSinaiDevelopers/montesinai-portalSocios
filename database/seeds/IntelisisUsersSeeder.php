<?php

use Illuminate\Database\Seeder;

class IntelisisUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		$this->call('LaratrustSeeder');

		$admin_user = App\User::where(['username' => 'administrator'])->first();

		$admin_user['usuario_intelisis'] = 'NESKENAZI';
		$admin_user['password_intelisis'] = '6f1db0a3906b8caaba9726763c5fd34c';

		$admin_user->save();
    }
}
