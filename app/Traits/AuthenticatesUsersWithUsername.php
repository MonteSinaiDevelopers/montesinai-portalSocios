<?php

namespace App\Traits;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

trait AuthenticatesUsersWithUsername {
	
	use AuthenticatesUsers {
		
	}
	
	public function username()
	{
		return 'username';
	}
	
}