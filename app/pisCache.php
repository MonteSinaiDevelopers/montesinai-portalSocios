<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pisCache extends Model
{
    //
	protected $table = 'pis_cache';
	
	protected $fillable = [
			'usuario_intelisis', 'referencia', 'params', 'resultado'
			];
	
	public $timestamps = false;
	
	public static function boot()
	{
		parent::boot();
		
		static::creating(function ($model) {
			$model->created_at = $model->freshTimestamp();
		});
	}
	
	public function params(){
		return $this->hasMany('App\pisCacheParam', 'cache_id', 'id');
	}
}
