<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Jenssegers\Date\Date;

class BMController extends Controller
{
    //

    function create(Request $request){
        $Acceso = session("acceso");

        if($Acceso == ""){
            return view('home')->withErrors("No se definio el usuario");
        }

        $params = [
                'Referencia' => 'Intelisis.pIS.DatosSocioFamilia',
                'Acceso' => $Acceso,
                'IDSocio' => $request->get("k"),
                'ValidarBM' => 1,
                'force' => 1,
        ];

        $result = pISRequest($params);

        if($result->success == 0){
            $OkRef = $result->msg;
            $request->session()->flash('alert-warning', sprintf('Ocurrio un error al obtener los datos. (%s)', $OkRef));
            return redirect('/');
        }

        $datos_kid = $result->row[0];

        // registrar EnProcesoBM en sesion y mostrar un elemento en Menu para poder regresar
        $request->session()->put('EnProcesoBM_idk', $request->get("k"));

        //dd($datos_kid);

        $bm_date = new Date($datos_kid['@FechaNacimiento']);

        $heb_date_string = jdtojewish(gregoriantojd($bm_date->month, $bm_date->day, $bm_date->year));

        list($heb_month_number, $heb_day, $heb_year) = explode('/', $heb_date_string);
        /*
        $heb_month_number = substr($heb_date_string, 0, strpos($heb_date_string, '/')); //mes, primera seccion de la fecha, del inicio a la primera diagonal
        $heb_day = substr($heb_date_string, strpos($heb_date_string, '/')+1, strpos($heb_date_string, '/', strpos($heb_date_string, '/')-1)); //dia, desde un caracter despues de la primera diagonal hasta la siguiente diagonal
        $heb_day = date('d', strtotime($heb_date_string));
        $heb_day = str_replace("/", "", $heb_day);
        
        $heb_year = substr($heb_date_string, strpos($heb_date_string, '/', strpos($heb_date_string, '/')+1)+1); //año, desde la segunda diagonal hasta el fin
        */

        $heb_months = [
            1  => 'Tishrei',
            2  => 'Jeshvan',
            3  => 'Kislev',
            4  => 'Tevet',
            5  => 'Shevat',
            6 => 'Adar I',
            7  => 'Adar II',
            8  => 'Nisan',
            9  => 'Iyar',
            10  => 'Sivan',
            11  => 'Tammuz',
            12  => 'Av',
            13  => 'Elul',
        ];
    
        $heb_month = $heb_months[$heb_month_number];

        $datos_kid['@FechaNacimientoHebreo'] = sprintf('%s/%s/%s', $heb_day, $heb_month, $heb_year);
        $datos_kid['@FechaBMHebreo'] = sprintf('%s/%s/%s', $heb_day, $heb_month, $heb_year+13);

        $datos_kid['@FechaBMComun'] = jdtogregorian(jewishtojd($heb_month_number, $heb_day, $heb_year+13));

        $FechaNac = new Date($datos_kid['@FechaNacimiento']);
        $datos_kid['@FechaNacimiento'] = $FechaNac->format('Y-m-d');

        $socio = session("socio_datos");

        if($socio->getF('@Sexo') == 'Masculino'){
            $datos_padre = [
                'BMPadreNombre' => $socio->getF('@Nombre'),
                'BMPadreApellidoPaterno' => $socio->getF('@APaterno'),
                'BMPadreApellidoMaterno' => $socio->getF('@AMaterno'),
                'BMPadreMail' => $socio->getF('@EMail'),
                'BMPadreTelefono' => $socio->getF('@TelefonoCelular'),
            ];

            $result_madre = pISRequest([
                'Referencia' => 'Intelisis.pIS.DatosSocioConyugue',
                'SubReferencia' => 'CONSULTA',
                'Acceso' => session('acceso'),
                'IDSocio' => $socio->getF('@ID_Socio'),
            ]);

            $datos_madre = [
                'BMMadreNombre' => $result_madre->getF('Nombre'),
                'BMMadreApellidoPaterno' => $result_madre->getF('APaterno'),
                'BMMadreApellidoMaterno' => $result_madre->getF('AMaterno'),
                'BMMadreMail' => $result_madre->getF('EMail'),
                'BMMadreTelefono' => $result_madre->getF('TelefonoCelular'),
            ];
        }
        else {
            $datos_padre = [
                'BMPadreNombre' => "",
                'BMPadreApellidoPaterno' => "",
                'BMPadreApellidoMaterno' => "",
                'BMPadreMail' => "",
                'BMPadreTelefono' => "",
            ];

            $result_padre = pISRequest([
                'Referencia' => 'Intelisis.pIS.DatosSocioConyugue',
                'SubReferencia' => 'CONSULTA',
                'Acceso' => session('acceso'),
                'IDSocio' => $socio->getF('@ID_Socio'),
            ]);

            $datos_madre = [
                'BMMadreNombre' => $result_padre->getF('Nombre'),
                'BMMadreApellidoPaterno' => $result_padre->getF('APaterno'),
                'BMMadreApellidoMaterno' => $result_padre->getF('AMaterno'),
                'BMMadreMail' => $result_padre->getF('EMail'),
                'BMMadreTelefono' => $result_padre->getF('TelefonoCelular'),
            ];
        }

        return view('barmitzvah.create')->with(compact(['datos_kid', 'datos_padre', 'datos_madre']));
    }

    function create_submit(Request $request){
        //validar acceso

        // validar archivo
        $validator = $this->validate($request, [
                'BMCreateNinoID_Socio' => 'required|integer',
                'BMCreateNinoNombre' => 'required|string',
                'BMCreateNinoApellidoPaterno' => 'required|string',
                'BMCreateNinoApellidoMaterno' => 'required|string',
                'BMCreateNinoFechaNac' => 'required|string',
                'BMCreateNinoEscolaridadPrimaria' => 'required|string',
                //'BMCreateNinoEscolaridadSecundaria' => 'string',
                'BMCreateNinoCLI' => 'required|string',

                'BMCreatePadreNombre' => 'required|string',
                'BMCreatePadreApellidoPaterno' => 'required|string',
                'BMCreatePadreApellidoMaterno' => 'required|string',
                'BMCreatePadreMail' => 'required|string',
                'BMCreatePadreTelefono' => 'required|string',

                'BMCreateMadreNombre' => 'required|string',
                'BMCreateMadreApellidoPaterno' => 'required|string',
                'BMCreateMadreApellidoMaterno' => 'required|string',
                'BMCreateMadreMail' => 'required|string',
                'BMCreateMadreTelefono' => 'required|string',

                'BMCreateTemploOpcion1' => 'required|string',
                'BMCreateTemploOpcion2' => 'required|string',
				'BMCreateNinoActa' => 'required|file',
        ]);

        //dd($request->all());

        //sacar direccion temporal del archivo en $_FILES
        $acta = $request->file('BMCreateNinoActa');
        //hacer otro request de upload en pIS
        $path = $acta->store(sprintf("BMActas/%s", $request->get('BMCreateNinoID_Socio')), "local");

        $result = pISRequest([
                'Referencia' => 'Intelisis.pIS.BarMitzvahPreSolicitud',
                'SubReferencia' => 'ALTA',
                'Acceso' => session('acceso'),
                'FechaCreacion' => date('Y-m-d hh:mm:ss'),
                'NinoID_Socio' => $request->get('BMCreateNinoID_Socio'),
                'NinoNombre' => $request->get('BMCreateNinoNombre'),
                'NinoApellidoPaterno' => $request->get('BMCreateNinoApellidoPaterno'),
                'NinoApellidoMaterno' => $request->get('BMCreateNinoApellidoMaterno'),
                'NinoFechaNac' => $request->get('BMCreateNinoFechaNac'),
                'NinoActaDireccionArchivo' => $path,
                'NinoEscolaridadPrimaria' => $request->get('BMCreateNinoEscolaridadPrimaria'),
                'NinoEscolaridadSecundaria' => $request->get('BMCreateNinoEscolaridadSecundaria'),
                'NinoCLI' => $request->get('BMCreateNinoCLI'),
                'PadreNombre' => $request->get('BMCreatePadreNombre'),
                'PadreApellidoPaterno' => $request->get('BMCreatePadreApellidoPaterno'),
                'PadreApellidoMaterno' => $request->get('BMCreatePadreApellidoMaterno'),
                'PadreMail' => $request->get('BMCreatePadreMail'),
                'PadreTelefono' => $request->get('BMCreatePadreTelefono'),
                'MadreNombre' => $request->get('BMCreateMadreNombre'),
                'MadreApellidoPaterno' => $request->get('BMCreateMadreApellidoPaterno'),
                'MadreApellidoMaterno' => $request->get('BMCreateMadreApellidoMaterno'),
                'MadreMail' => $request->get('BMCreateMadreMail'),
                'MadreTelefono' => $request->get('BMCreateMadreTelefono'),
                'TemploOpcion1' => $request->get('BMCreateTemploOpcion1'),
                'TemploOpcion2' => $request->get('BMCreateTemploOpcion2'),
                'force' => 1,
            ]);
//dd($result->success);
        if($result->success == 0){
            $request->session()->flash('alert-warning', sprintf('Ocurrio un error al enviar el formulario (%s)', $result->msg));
            return redirect(sprintf('barmitzvah/new?k=%s', $request->get('BMCreateNinoID_Socio')));
        }
        else {
           $request->session()->flash('alert-success', 'El formulario fue enviado exitósamente');
    	   return view('barmitzvah.submit');
        }
    }
}
