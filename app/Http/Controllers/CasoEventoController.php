<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class CasoEventoController extends Controller
{
    //
    private $select_fields = [
        "Estatus",
        "TipoEvento",
    ];

    function getJSON(Request $request){
    	if($request->input('casoid') == ''){
    		return response()->json(['success' => 0, 'msg' => 'No se definio el ID de caso']);
    	}
    	else {
    		return response()->json(['msg_no_data' => 'No hay Eventos', 'success' => 1, 'eventos' => $this->_getEventosList($request->input('casoid'), $request->get('force', false))]);
    	}
    }
    
    private function _getEventosList($casoid, $force = false){
    	$params = [
    		'Referencia' => 'Intelisis.pIS.ListaCasoEventos',
    		'IDCaso' => $casoid,
    	];
        if($force != false){
            $params['force'] = time();
        }

    	$Resultado = pISRequest($params);

    	if(!isset($Resultado->row) || !is_array($Resultado->row) || empty($Resultado->row)){
    		return array();
    	}

        $params_directivos = [
            'Referencia' => 'Intelisis.pIS.ListaDirectivos',
            'SubReferencia' => 'CONSULTA'
        ];

        $Resultado_directivos = pISRequest($params_directivos);

        $directivos = array();
        foreach($Resultado_directivos->row as $directivo){
            $directivos[] = ['ID' => $directivo['@ID'], 'Nombre' => $directivo['@Nombre'], 'Usuario' => $directivo['@Usuario']];
        }
    	
    	$casoeventos = array();
    	foreach($Resultado->row as $casoevento){
            foreach($this->select_fields as $field){
                if(isset($casoevento['@'.$field]) && $casoevento['@'.$field] != '') {
                    $casoevento[$field.$casoevento['@'.$field]] = $casoevento['@'.$field];
                }
                else {
                    $casoevento[$field.'NULL'] = 'NULL';
                }
            }
            $casoevento['ListaDirectivos'] = $directivos;
            foreach($casoevento['ListaDirectivos'] as $key => $value){
                if($value['ID'] == $casoevento['@Responsable']){
                    $casoevento['ListaDirectivos'][$key]['selected'] = 'selected';
                }
            }
    		$casoeventos[] = $casoevento;
    	}
    	
    	return $casoeventos;
    }
    
    function deleteJSON(Request $request){
    	if($request->input('casoeventoid') == ""){
    		return response()->json(["success" => 0, "msg" => "No definio un ID de Caso Evento"]);
    	}
    	
    	$params = [
    		'Referencia' => 'Intelisis.pIS.CasoEvento',
    		'SubReferencia' => 'BAJA',
    		'IDEvento' => $request->input('casoeventoid'),
    	];
    	$Resultado = pISRequest($params);

    	if($Resultado->success == 0){
    		return response()->json(["success" => $Resultado->success0, "msg" => $Resultado->msg]);
    	}
    	else {
    		return response()->json(["success" => $Resultado->success, "msg" => "Evento eliminado exitosamente"]);
    	}
    }

    function editJSON(Request $request){
        $params = [
            'Referencia' => 'Intelisis.pIS.CasoEvento',
            'SubReferencia' => 'CAMBIO',
            'IDEvento' => $request->input('casoeventoid'),
            'Estatus' => $request->input('val'),
            'Forzar' => time(),
        ];

        $Resultado = pISRequest($params);

        return response()->json(["success" => $Resultado->success, "msg" => $Resultado->msg]);
    }

    function createJSON(Request $request){
        $request_fields = [
            'CasoEventoCreateCasoId' => 'required|numeric',
            'CasoEventoCreateTitulo' => 'required|string',
            'CasoEventoCreateTipoEvento' => 'required|string|in:Junta,Llamada,Evento,Otros',
            'CasoEventoCreateFechaEvento' => 'required|date',
            'CasoEventoCreateResponsable' => 'required|numeric',
            'CasoEventoCreateDetalles' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $request_fields);

        if ($validator->fails()) {    
            return response()->json(['success' => 0, 'errors' => $validator->messages()], 200);
        }

        $params = [
            'Referencia' => 'Intelisis.pIS.CasoEvento',
            'SubReferencia' => 'ALTA',
            'IDCaso' => $request->get('CasoEventoCreateCasoId'),
            'Titulo' => $request->get('CasoEventoCreateTitulo'),
            'TipoEvento' => $request->get('CasoEventoCreateTipoEvento'),
            'Estatus' => 'SINAFECTAR',
            'FechaEvento' => $request->get('CasoEventoCreateFechaEvento'),
            'Responsable' => $request->get('CasoEventoCreateResponsable'),
            'Detalles' => $request->get('CasoEventoCreateDetalles'),
            'AsisteSolicitante' => $request->get('CasoEventoCreateAsisteSolicitante', false),
            'AsisteContraparte' => $request->get('CasoEventoCreateAsisteContraparte', false),
        ];

        $casoevento = pISRequest($params);
        $casoevento = $casoevento->row[0];
        return response()->json(['success' => 1, 'casoevento' => [
                '@ID' => $casoevento['@ID'],
                '@Titulo' => $request->get('CasoEventoCreateTitulo'),
                '@TipoEvento' => $request->get('CasoEventoCreateTipo'),
                '@FechaH' => $request->get('CasoEventoCreateFechaEvento'),
                '@Responsable' => $request->get('CasoEventoCreateResponsable'),
                '@Detalles' => $request->get('CasoEventoCreateDetalles'),
            ]
        ]);
    }
}
