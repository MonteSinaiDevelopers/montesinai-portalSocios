<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AnexoController extends Controller
{
    //
    function download(Request $request){
    	$Acceso = $request->get("Acceso");
    	if($Acceso == ""){
    		return response()->json(["success" => 0, "msg" => "No se definio el usuario"]);
    	}

    	$IDR = $request->get("IDR");
    	if($IDR == ""){
    		return response()->json(["success" => 0, "msg" => "No se definio el anexo a descargar"]);
    	}

    	$params = [
    		'Referencia' => 'Intelisis.pIS.ObtenerAnexoMov',
    		'IDR' => $IDR,
    		'Acceso' => $Acceso,
    		'RequestURI' => 'spIntelisisServiceObtenerAnexoMov'
    	];

    	$result = pISRequest($params);

        if($result->success == 0){
            return redirect('socio/anexo')->withMessage("Ocurrió un error al obtener el archivo");
        }

        header("Content-type: application/octet-stream");
        header("Cache-Control: no-store, no-cache");
        header(sprintf("Content-Disposition: %s", $result->disposition));

        $output_stream = fopen('php://output','w');
        fwrite($output_stream, $result->filestream);
        exit;
    }
}
