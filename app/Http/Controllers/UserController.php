<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    function getUserListJSON(){
    	$users = array();
    	foreach(User::all() as $user){
    		$users[] = $user;
    	}
    	
    	return response()->json(["msg_no_data" => "No hay Usuarios", "success" => 0, "users" => $users]);
    }
    
    function get(Request $request){
    	if($request->input("userid") == "" && !is_numeric($request->input("userid"))){
    		return $this->getUserListJSON();
    	}
    	else{
    		return response()->json(User::get(["id" => $request->input("userid")])->first());
    	}
    }
    
    function edit(Request $request){
    	if($request->input("userid") == ""){
    		return view('user/admin')->withErrors("No se definio el ID de Usuario");
    	}
    	
    	$user = User::find($request->input("userid"));
    	
    	return view('user/edit')->with(compact('user'));
    }
    
    function modify(Request $request){
    	if($request->input("userid") == ""){
    		return view('user/admin')->withErrors("No se definio el ID de Usuario");
    	}
    	
    	$validator = $this->validate($request, [
    			'name' => 'required|string',
    			'username' => 'required|string',
    			'email' => 'required|email',
    			'password' => 'present|confirmed',
    	]);
    	
    	$user = User::find($request->input("userid"));
    	
    	$user->name = $request->input("name");
    	$user->username = $request->input("username");
    	$user->email = $request->input("email");
    	$user->usuario_intelisis = $request->input("usuario_intelisis");
    	$user->password_intelisis = $request->input("password_intelisis");
    	
    	if($request->input("password") != ""){
    		$user->password= Hash::make($request->input("password"));
    	}

        foreach(array_keys($request->get("userperm-role", array())) as $rolename){
            try {
                if(!$user->hasRole($rolename)){
                    $user->attachRole($rolename);
                }
            }
            catch(Exception $e){
                return redirect('user/edit?userid'.$request->input("userid"))->with(compact('user'))->withErrors("Ocurrió un error al modificar los roles del usuario");
            }
        }

        $user->detachPermissions($user->allPermissions());
        foreach($request->get("userperm", array()) as $role=> $perms){
            foreach($perms as $perm => $status){
                if(!$user->hasPermission($perm)){
                    try {
                        $user->attachPermission($perm);
                    }
                    catch(Exception $e){
                        return redirect('user/edit?userid'.$request->input("userid"))->with(compact('user'))->withErrors("Ocurrió un error al modificar los permisos del usuario");
                    }
                }
            }
        }        

        foreach(Role::all() as $role){
            if($user->hasRole($role->name) && !array_key_exists($role->name, $request->get("userperm-role"))){
                $user->detachRole($role->name);
            }

            foreach($role->permissions()->get() as $permission){
                if($user->hasPermission($permission->name) && !in_array($permission->name, $request->get("userperm"))){
                    $user->detachPermission($permission->name);
                }
            }
        }

        try {
            $user->save();
        }
        catch(Exception $e){
            return redirect('user/edit?userid'.$request->input("userid"))->with(compact('user'))->withErrors("Ocurrió un error al modificar el usuario");
        }

    	$request->session()->flash('alert-success', 'Se modificó el usuario exitósamente');
    	return redirect('user/admin');
    }
    
    function getIntelisisUsersJSON(){   	
        $params = [
            'Referencia' => 'Intelisis.pIS.ListaUsuarios',
        ];

        $Resultado = pISRequest($params);
    	
        if($Resultado->success == 0){
            return response()->json([
                'success' => 0,
                'msg' => $Resultado->msg,
            ]);
        }

    	$users = array();
    	if(!empty($Resultado->row)){
    		foreach($Resultado->row as $usuario){
    			$users[] = $usuario;
    		}
    	}
    	
    	return response()->json([
    		'success' => 1,
    		'Usuarios' => $users,
    	]);
    }

    function editProfile(){
        $user = Auth::user();

        return view('user/profile_edit')->with(compact(['user']));
    }

    function modifyProfile(Request $request){
        $userid = Auth::id();
        $validator = $this->validate($request, [
            'name' => 'required|string',
            'username' => 'required|string|unique:users,name,'.$userid,
            'email' => 'required|email',
            'password' => 'present|confirmed',
        ]);
        
        $user = User::find($userid);
        
        $user->name = $request->input("name");
        $user->username = $request->input("username");
        $user->email = $request->input("email");
        
        if($request->input("password") != ""){
            $user->password= Hash::make($request->input("password"));
        }

        try {
            $user->save();
        }
        catch(Exception $e){
            return redirect('profile/edit')->with(compact('user'))->withErrors("Ocurrió un error al modificar el usuario");
        }

        $request->session()->flash('alert-success', 'Se modificó el usuario exitósamente');
        return redirect('profile/edit');
    }
}
