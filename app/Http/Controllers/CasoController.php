<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class CasoController extends Controller
{
    private $select_fields = [
        "Estatus",
    ];

    private $validate_fields = array(
                1 => [
                "PhaseName" => "SolicitanteDatos",
                "PhaseView" => "caso.new_SolicitanteDatos",
                "fields" => [
                    'CasoCreateAcceso' => 'required|string',
                    'CasoCreateSolicitanteNombre' => 'required|string',
                    'CasoCreateSolicitanteApellidoPaterno' => 'required|string',
                    'CasoCreateSolicitanteApellidoMaterno' => 'required|string',
                    'CasoCreateSolicitanteFechaNac' => 'required|date',
                    'CasoCreateSolicitanteDireccionCasa' => 'required|string',
                    'CasoCreateSolicitanteColoniaCasa' => 'required|string',
                    'CasoCreateSolicitanteDelegacionCasa' => 'required|string',
                    'CasoCreateSolicitantePoblacionCasa' => 'required|string',
                    'CasoCreateSolicitanteEstadoCasa' => 'required|string',
                    'CasoCreateSolicitantePaisCasa' => 'required|string',
                    'CasoCreateSolicitanteCodigoPostalCasa' => 'required|digits:5',
                    'CasoCreateSolicitanteTelefonoCasa' => 'required|min:8',
                    'CasoCreateSolicitanteTelefonoContacto' => 'required|min:8',
                    'CasoCreateSolicitanteCorreo' => 'required|email',
                    ],
                ],
                2 => [
                "PhaseName" => "SolicitanteTrabajo",
                "PhaseView" => "caso.new_SolicitanteTrabajo",
                "fields" => [
                    'CasoCreateAcceso' => 'required|string',
                    'CasoCreateSolicitanteNombreTrabajo' => 'string',
                    'CasoCreateSolicitantePuesto' => 'string',
                    'CasoCreateSolicitanteDireccionTrabajo' => 'string',
                    'CasoCreateSolicitanteColoniaTrabajo' => 'string',
                    'CasoCreateSolicitanteDelegacionTrabajo' => 'string',
                    'CasoCreateSolicitantePoblacionTrabajo' => 'string',
                    'CasoCreateSolicitanteEstadoTrabajo' => 'string',
                    'CasoCreateSolicitantePaisTrabajo' => 'string',
                    'CasoCreateSolicitanteCodigoPostalTrabajo' => 'digits:5',
                    'CasoCreateSolicitanteTelefonoTrabajo' => 'min:8',
                    ],
                ],
                3 => [
                "PhaseName" => "SolicitanteEducacion",
                "PhaseView" => "caso.new_SolicitanteEducacion",
                "fields" => [
                    'CasoCreateAcceso' => 'required|string',
                    'CasoCreateSolicitanteEscKinder' => 'string',
                    'CasoCreateSolicitanteEscPrimaria' => 'string',
                    'CasoCreateSolicitanteEscSecundaria' => 'string',
                    'CasoCreateSolicitanteEscPreparatoria' => 'string',
                    'CasoCreateSolicitanteEscUniversidad' => 'string',
                    'CasoCreateSolicitanteEscCarrera' => 'string',
                    'CasoCreateSolicitanteEscOtros' => 'string',
                    ],
                ],
                4 => [
                "PhaseName" => "ContraparteDatos",
                "PhaseView" => "caso.new_ContraparteDatos",
                "fields" => [
                    'CasoCreateAcceso' => 'required|string',
                    'CasoCreateContraparteNombre' => 'string',
                    'CasoCreateContraparteApellidoPaterno' => 'string',
                    'CasoCreateContraparteApellidoMaterno' => 'string',
                    'CasoCreateContraparteFechaNac' => 'nullable|date',
                    'CasoCreateContraparteDireccionCasa' => 'nullable|string',
                    'CasoCreateContraparteColoniaCasa' => 'nullable|string',
                    'CasoCreateContraparteDelegacionCasa' => 'nullable|string',
                    'CasoCreateContrapartePoblacionCasa' => 'nullable|string',
                    'CasoCreateContraparteEstadoCasa' => 'nullable|string',
                    'CasoCreateContrapartePaisCasa' => 'nullable|string',
                    'CasoCreateContraparteCodigoPostalCasa' => 'nullable|digits:5',
                    'CasoCreateContraparteTelefonoCasa' => 'nullable|min:8',
                    'CasoCreateContraparteTelefonoContacto' => 'min:8',
                    'CasoCreateContraparteCorreo' => 'nullable|email',
                    ],
                ],
                5 => [
                "PhaseName" => "ContraparteTrabajo",
                "PhaseView" => "caso.new_ContraparteTrabajo",
                "fields" => [
                    'CasoCreateAcceso' => 'required|string',
                    'CasoCreateContraparteNombreTrabajo' => 'string',
                    'CasoCreateContrapartePuesto' => 'string',
                    'CasoCreateContraparteDireccionTrabajo' => 'nullable|string',
                    'CasoCreateContraparteColoniaTrabajo' => 'nullable|string',
                    'CasoCreateContraparteDelegacionTrabajo' => 'nullable|string',
                    'CasoCreateContrapartePoblacionTrabajo' => 'nullable|string',
                    'CasoCreateContraparteEstadoTrabajo' => 'nullable|string',
                    'CasoCreateContrapartePaisTrabajo' => 'nullable|string',
                    'CasoCreateContraparteCodigoPostalTrabajo' => 'nullable|digits:5',
                    'CasoCreateContraparteTelefonoTrabajo' => 'nullable|min:8',
                    ],
                ],
                6 => [
                "PhaseName" => "ContraparteEducacion",
                "PhaseView" => "caso.new_ContraparteEducacion",
                "fields" => [
                    'CasoCreateAcceso' => 'required|string',
                    'ContraparteEscKinder' => 'nullable|string',
                    'CasoCreateContraparteEscPrimaria' => 'nullable|string',
                    'CasoCreateContraparteEscSecundaria' => 'nullable|string',
                    'CasoCreateContraparteEscPreparatoria' => 'nullable|string',
                    'CasoCreateContraparteEscUniversidad' => 'nullable|string',
                    'CasoCreateContraparteEscCarrera' => 'nullable|string',
                    'CasoCreateContraparteEscOtros' => 'nullable|string',
                    ],
                ],
                7 => [
                "PhaseName" => "CasoDetalles",
                "PhaseView" => "caso.new_CasoDetalles",
                "fields" => [
                    'CasoCreateAcceso' => 'required|string',
                    'CasoCreateCasoDetalles' => 'required|string',
                    ],
                ],
                8 => [
                "PhaseName" => "Submit",
                "PhaseView" => 'caso.new_Submit'
                ],
            );

    //
    function get(Request $request){
    	if($request->input("casoid") == ""){
    		return view('caso.admin');
    	}
    	else {
    		$caso = $this->_getCaso($request->input("casoid"));

    		if(empty($caso)){
    			return redirect('home')->withErrors(['Ocurrio un error al obtener los detalles del caso']);
    		}
    		$casoid = $request->input("casoid"); //Tengo que hacer esto porque por alguna razon el app('request')->input('casoid') se esta sobreescribiendo a 1

            $params_directivos = [
                'Referencia' => 'Intelisis.pIS.ListaDirectivos',
                'SubReferencia' => 'CONSULTA'
            ];

            $Resultado_directivos = pISRequest($params_directivos);

            $directivos = $Resultado_directivos->row;

    		return view('caso.view')->with(compact('casoid', 'caso', 'directivos'));
    	}
    }
    
    function getJSON(Request $request){
    	if($request->input("casoid") == ""){
    		return response()->json($this->_getCasoList($request->get('force', false)));
    	}
    	else {
    		return response()->json($this->_getCaso($request->input("casoid")));
    	}
    }
    
    function admin(){
    	return view('caso.admin');
    }
    
    private function _getCaso($casoid){
    	$params = [
    		'Referencia' => 'Intelisis.pIS.Caso',
    		'IDCaso' => $casoid,
    		'SubReferencia' => 'CONSULTA',
    	];
    	$Resultado = pISRequest($params);

    	if(!isset($Resultado->row)){
    		return array();
    	}
    	
    	return $Resultado->row;
    }
    
    private function _getCasoList($force = false){
    	$params = [
    			'Referencia' => 'Intelisis.pIS.ListaCasos',
    	];
        if($force != false){
            $params['force'] = time();
        }

		$Resultado = pISRequest($params);
    	
    	$casos = array();
    	if(isset($Resultado->row) || !empty($Resultado->row)){
            // Hack para manejar los resultados de row con una sola row, ya que json_decode() me lo regresa como un arreglo en vez de un arreglo adentro de un arreglo con un solo elemento
            if(!isset($Resultado->row[0]) || !is_array($Resultado->row[0])){
                $casos[] = $Resultado->row;
            }
            else {
        		foreach($Resultado->row as $caso){
                    foreach($this->select_fields as $field){
                        if(isset($caso['@'.$field]) && $caso['@'.$field] != '') {
                            $caso[$field.$caso['@'.$field]] = $caso['@'.$field];
                        }
                        else {
                            $caso[$field.'NULL'] = 'NULL';
                        }
                    }
        			$casos[] = $caso;
        		}  
            }
    	}
    	
    	return array("success" => 0, "casos" => $casos);
    }

    private function createValidateFormByPhase($request, $phase){
        if(!isset($this->validate_fields[$phase])){
            return view('caso.new_ContraparteDatos')->withErrors(['Ocurrio un error en las fases del formulario']);
        }

        $session_vars = $request->session()->get('CasoNew');
        $form_error = false;
        $form_errors = [];

        $messages = [
            'required' => 'El campo :attribute es requerido'
        ];

        //Ya legamos a la fase final? Validar todos los campos de todas las fases.
        if($this->validate_fields[$phase]['PhaseName'] == "Submit"){
            $validate_fields_all = [];

            foreach($this->validate_fields as $phase => $phasedata){
                if(isset($this->validate_fields[$request->input('CasoCreatePhase', 0)]['fields']) && is_array($this->validate_fields[$request->input('CasoCreatePhase', 0)]['fields'])){
                    $validate_fields_all = array_merge($validate_fields_all, $phasedata['fields']);
                }
            }

            $validator = Validator::make($request->session()->all(), $validate_fields_all, $messages);

            if($validator->fails()){
                return view('caso.new_ContraparteDatos')->withErrors(['Ocurrio un error al intentar enviar el formulario']);
            }

            $params =[
                'Referencia' => 'Intelisis.pIS.Caso',
                'SubReferencia' => 'ALTA',
                'Caso' => $request->session()->get('CasoNew', array()),
            ];

            try {
                $result = pISRequest($params);
            }
            catch (Exception $e){
                //ToDo: logear el error regresado de la solicitud para debugging.
                return view($this->validate_fields[max(array_keys($this->validate_fields))]['PhaseView'])->withErrors(['Ocurrio un error al intentar enviar el fomulario']);
            }

            if($result->success == 0){
                return view($this->validate_fields[max(array_keys($this->validate_fields))]['PhaseView'])->withErrors([$result->msg]);
            }

            return redirect('/')->with('alert-success', 'Se envió la Solicitud exitósamente');
        }

        //Si estamos aqui, es una fase que no es la final (marcada con Phasename = "Submit"). Validar solo los campos de esa fase
        $validator = Validator::make($request->all(), $this->validate_fields[$phase]['fields'], $messages);
        if ($validator->fails()) {
            return view($this->validate_fields[$phase]['PhaseView'])->withErrors($validator);//->withInput();
        }

        //ToDo: en vez de volver a ciclar por cada field, ciclar los regresados como errores en $validator. Quitar el return de arriba
        /*
        foreach($this->validate_fields[$phase]['fields'] as $field_name => $validation){
            if($request->input($field_name) == ''){
                $form_error = true;
                $form_errors[$field_name.'HasError'] = 'has-error';
            }
        }
        */

        if($form_error){
            return view($this->validate_fields[$phase]['PhaseView'])->with(compact(['form_errors']))->withErrors(['Los campos marcados en rojo son obligatorios']);
        }
        else {
            $phase++;
            return view($this->validate_fields[$phase]['PhaseView']);
        }
    }
    
    private function create_getForm($request, $resultado_socio, $phase){
        if(isset($this->validate_fields[$request->input('CasoCreatePhase', 0)]['fields']) && is_array($this->validate_fields[$request->input('CasoCreatePhase', 0)]['fields'])){
    		foreach($this->validate_fields[$request->input('CasoCreatePhase', 0)]['fields'] as $varname => $rules){
                $request->session()->pull('CasoNew.'.$varname);
                $request->session()->put('CasoNew.'.$varname, $request->get($varname));
    		}
        }

        return $this->createValidateFormByPhase($request, $phase);
    }
    
    function create(Request $request){
        $Acceso = $request->session()->get('acceso', '0');
        $phase = $request->input('CasoCreatePhase', 0);

    	if($Acceso != '0'){
            $resultado_socio = pISRequest([
                    'Referencia' => 'Intelisis.pIS.Socio',
                    'SubReferencia' => 'CONSULTA',
                    'Acceso' => $Acceso,
                ]);

    		if($request->input('CasoCreatePhase', 0) > 0){
    			return $this->create_getForm($request, $resultado_socio, $phase)->with(compact('resultado_socio'));
    		}
    		else {
                return view('caso.new_SolicitanteDatos')->with(compact('resultado_socio'));
    		}
    	}
    	else {
    		return redirect()->route('home')->withErrors(['No cuenta con un Acceso válido']);
    	}
    }

    function asignarJSON(Request $request){
        $id_obj = $request->get('obj', '');
        $id_dir = $request->get('dir', '');
        $id_caso = $request->get('id_caso', '');

        if($id_obj == ''){
            return response()->json(['success' => 0, 'msg' => 'No se indico la posicion a asignar (id_obj)']);            
        }

        if($id_dir == ''){
            return response()->json(['success' => 0, 'msg' => 'No se indico la persona a asignar (id_dir)']);
        }

        $params = [
            'Referencia' => 'Intelisis.pIS.CasoAsignar',
            'SubReferencia' => 'CAMBIO',
            'id_obj' => $id_obj,
            'id_dir' => $id_dir,
            'id_caso' => $id_caso,
        ];

        $result = pISRequest($params);

        return response()->json([
            'success' => $result->success,
            'msg' => $result->msg,
        ]);
    }

    function getAnexosJSON(Request $request){
        $params = [
            'Referencia' => 'Intelisis.pIS.CasoAnexos',
            'SubReferencia' => 'CONSULTA',
            'IDCaso' => $request->get('IDCaso')
        ];

        $result = pISRequest($params);

        return response()->json([
            'success' => $result->success,
            'Anexos' => $result->row,
        ]);
    }

    function getAnexo(Request $request){
        //checar permiso de Casos

        $IDR = $request->get("IDR");
        if($IDR == ""){
            return response()->json(["success" => 0, "msg" => "No se definio el anexo a descargar"]);
        }

        $params = [
            'Referencia' => 'Intelisis.pIS.CasoObtenerAnexoMov',
            'IDR' => $IDR,
            'RequestURI' => 'spIntelisisServiceObtenerAnexoMov'
        ];

        $result = pISRequest($params);

        if($result->success == 0){
            return redirect('caso/admin')->withMessage("Ocurrió un error al obtener el archivo");
        }

        header("Content-type: application/octet-stream");
        header("Cache-Control: no-store, no-cache");
        header(sprintf("Content-Disposition: %s", $result->disposition));

        $output_stream = fopen('php://output','w');
        fwrite($output_stream, $result->filestream);
        exit;
    }

    function actualizarJSON(Request $request){
        $estatus = $request->get("Estatus", "");
        $conclusion = $request->get("Conclusion", "");
        $idcaso = $request->get('idcaso', '');

        if($idcaso == ""){
            return response()->json(['success' => 0, 'msg' => 'No se indicó un ID de caso válido']);
        }

        if($estatus == "" || !in_array($estatus, ['CONCLUIDO', 'VIGENTE', 'PENDIENTE', 'CANCELADO', 'SINAFECTAR'])){
            return response()->json(['success' => 0, 'msg' => sprintf('El Estatus indicado %s no es valido', $estatus)]);
        }

        if($estatus == 'CONCLUIDO' && trim($conclusion) == ""){
            return response()->json(['success' => 0, 'msg' => 'Debe capturar una conclusión cuando se mueve el Estatus a CONCLUIDO']);
        }

        $params = [
            'Referencia' => 'Intelisis.pIS.Caso',
            'SubReferencia' => 'CAMBIO',
            'Estatus' => $estatus,
            'Conclusion' => $conclusion,
            'IDCaso' => $idcaso,
        ];

        $result = pISRequest($params);

        return response()->json([
            'success' => $result->success,
            'msg' => $result->msg,
        ]);
    }
}
