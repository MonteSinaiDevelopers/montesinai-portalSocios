<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SocioController extends Controller
{
    //
    function getAnexosAsJSON(){
    	$Acceso = session("acceso");
    	if($Acceso == ""){
    		return view('/')->withErrors("No se definio el usuario");
    	}
    	
    	$params = [
    			'Referencia' => 'Intelisis.pIS.SocioAnexos',
    			'Acceso' => $Acceso,
    	];
    	$Resultado = pISRequest($params);
    	
    	if(!isset($Resultado->row) || empty($Resultado->row)){
    		return array("msg_no_data" => "No hay Archivos para el socio", "success" => 0);
    	}
    	
    	$Anexos = array();
    	foreach($Resultado->row as $row){
    		$Anexos[] = $row;
    	}
    	return array("msg_no_data" => "No hay Archivos para el socio", "success" => 1, "Anexos" => $Anexos);
    }

    function getListaAnexos(){
    	$Acceso = session("acceso");
    	if($Acceso == ""){
    		return view('/')->withErrors("No se definio el usuario");
    	}
    	
    	return view('socio.lista_anexos', compact(['Acceso']));
    }

    function getListaJSON(){
        ini_set('max_execution_time', 600);
        set_time_limit(0);
        $params = [
            'Referencia' => 'Intelisis.pIS.SocioLista',
            'SubReferencia' => 'LISTA',
            'force' => 1,
        ];
        $Resultado = pISRequest($params);

        return response()->json(array("msg_no_data" => "No hay Socios", "success" => 1, "Socios" => $Resultado->row));
    }

    function admin(){
        return view('socio.admin');
    }
}
