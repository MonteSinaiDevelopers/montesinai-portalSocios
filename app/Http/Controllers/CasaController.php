<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CasaController extends Controller
{
    //
    function view(Request $request){
    	$params = [
    		"Referencia" => 'Intelisis.pIS.Casa',
    		"SubReferencia" => 'CONSULTA',
    		"ID_Casa" => $request->get("id_casa", "")
    	];
    	$result = pISRequest($params);

    	$casa = [];
    	if(!isset($result->row) or !is_array($result->row) || empty($result->row)){
    		return redirect('/')->withErrors(['Ocurrio un error al obtener los datos de la casa']);
    	}
    	foreach($result->row as $row){
    		$casa[] = $row;
    	}

    	return view('casa.view')->with(compact('casa'));
    }
}
