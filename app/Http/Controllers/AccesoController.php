<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Route;

class AccesoController extends Controller
{
    //
    function main(Request $request){
    	$Acceso = $request->input("Acceso");
    	if($Acceso == ""){
    		return redirect('home')->withErrors(["No se definio el usuario"]);
    	}

    	session(['acceso' => $Acceso]);
    	
    	$redirect = $request->input('r', '/');
        $redirect_name = substr($redirect, 0, strpos($redirect, '?')); //remover parametros GET de la URL para matchear con nombres de ruta

    	if(Route::has($redirect_name) == false){
    		$redirect = '/';
    	}

        $resultado_socio = pISRequest([
                'Referencia' => 'Intelisis.pIS.Socio',
                'SubReferencia' => 'CONSULTA',
                'Acceso' => $Acceso,
            ]);

        if($resultado_socio->success == 0){
            if(isset($resultado_socio->msg) && trim($resultado_socio->msg) != ''){
                return redirect('/')->withErrors([$resultado_socio->msg]);
            }
            else {
                return redirect('/')->withErrors(['Ocurrio un error al conectarse a la base de datos']);
            }
        }
        else {
            session(['socio_datos' => $resultado_socio]);
    	    return redirect($redirect)->with('alert-success', 'Acceso validado. Expira el '.date('d/M/Y H:i', strtotime($resultado_socio->row[0]['@AccesoExpira'])));
        }
    }
}
