<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EstadoCuentaController extends Controller
{
    //
    function getAsJSON(){
    	$id_socio = request("id_socio", 0);
    	if($id_socio == ""){
    		return view('home')->withErrors("No se definio el socio");
    	}
    	
    	$params = [
    			'Referencia' => 'Intelisis.pIS.MonteSinaiEstadoDeCuenta',
    			'IDSocio' => $id_socio,
    	];
    	$Resultado = pISRequest($params);
    	
    	if(!isset($Resultado->row) || empty($Resultado->row)){
    		return array("msg_no_data" => "No hay movimientos del socio", "success" => 1, "msg" => "No hay movimientos para el socio");
    	}
    	
    	$EstadoCuentaDetalle = array();
    	foreach($Resultado->row as $row){
    		$EstadoCuentaDetalle[] = $row;
    	}
    	return array("msg_no_data" => "No hay movimientos del socio", "success" => 1, "EstadoCuentaDetalle" => $EstadoCuentaDetalle);
    }

    function getFromGET(){
        $id_socio = request("id_socio", "");
        if(!Auth::user()->can('read-socioscxc')){
            return view('home')->withErrors("No tiene permiso");
        }

        if($id_socio == ""){
            return view('home')->withErrors("No se definio el socio");
        }
        
        return view('socio.estado_cuenta', compact(['id_socio']));
    }

    function getEstadoCuentaAccesoJSON(){
        $Acceso = session("acceso");
        if($Acceso == ""){
            return view('/')->withErrors("No se definio el usuario");
        }
        
        $params = [
                'Referencia' => 'Intelisis.pIS.MonteSinaiEstadoDeCuenta',
                'Acceso' => $Acceso,
        ];
        $Resultado = pISRequest($params);
        
        if(!isset($Resultado->row) || empty($Resultado->row)){
            return array("msg_no_data" => "No hay movimientos del socio", "success" => 1, "msg" => "No hay movimientos para el socio");
        }
        
        $EstadoCuentaDetalle = array();
        foreach($Resultado->row as $row){
            $EstadoCuentaDetalle[] = $row;
        }
        return array("msg_no_data" => "No hay movimientos del socio", "success" => 1, "EstadoCuentaDetalle" => $EstadoCuentaDetalle);
    }

    function getEstadoCuentaAcceso(){
        $Acceso = session("acceso");
        if($Acceso == ""){
            return view('/')->withErrors("No se definio el usuario");
        }
        
        return view('socio.estado_cuenta_socio', compact(['Acceso']));
    }

    function getCxCPendientes(Request $request){
        if(Auth::user()->can('read-socioscxc') && $request->get('id_socio', '') != ''){
            $id_socio = $request->get('id_socio');
            return view('socio.cxc_pendientes', compact(['id_socio']));
        }

        return view('home')->withErrors("No tiene permiso");
    }

    function getCxCPendientesAcceso(Request $request){
        $Acceso = session("acceso");
        if($Acceso == ""){
            return view('/')->withErrors("No se definio el usuario");
        }
        
        return view('socio.cxc_pendientes_socio', compact(['Acceso']));
    }

    function getCxCPendientesAccesoJSON(){
        $Acceso = session("acceso");
        if($Acceso == ""){
            return view('/')->withErrors("No se definio el usuario");
        }
        
        $params = [
                'Referencia' => 'Intelisis.pIS.CxCPendientes',
                'Acceso' => $Acceso,
        ];
        $Resultado = pISRequest($params);
        
        if(!isset($Resultado->row) || empty($Resultado->row)){
            return array("msg_no_data" => "No hay movimientos del socio", "success" => 1, "msg" => "No hay movimientos para el socio");
        }
        
        $CxCPendientes = array();
        foreach($Resultado->row as $row){
            $CxCPendientes[] = $row;
        }
        return array("msg_no_data" => "No hay movimientos del socio", "success" => 1, "CxCPendientes" => $CxCPendientes);
    }

    function getCxCPendientesJSON(){
        $id_socio = request("id_socio", 0);
        if($id_socio == 0){
            return view('home')->withErrors("No se definio el socio");
        }
        
        $params = [
                'Referencia' => 'Intelisis.pIS.CxCPendientes',
                'IDSocio' => $id_socio,
        ];
        $Resultado = pISRequest($params);
        
        if(!isset($Resultado->row) || empty($Resultado->row)){
            return array("msg_no_data" => "No hay movimientos del socio", "success" => 1, "msg" => "No hay movimientos para el socio");
        }
        
        $CxCPendientes = array();
        foreach($Resultado->row as $row){
            $CxCPendientes[] = $row;
        }
        return array("msg_no_data" => "No hay movimientos del socio", "success" => 1, "CxCPendientes" => $CxCPendientes);
    }
}
