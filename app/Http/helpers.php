<?php 

use App\User;
use App\pisCache;

function isAssoc(array $arr)
{
    if (array() === $arr) return false;
    return array_keys($arr) !== range(0, count($arr) - 1);
}

class pISResultado{
    public $success = 0;
    public $msg = '';
    public $disposition = '';
    public $filestream = false;
    public $row = array();

    public function __construct($params = array()){
        foreach($params as $param => $value){
            if(isset($this->$param)){
                if($param == 'row' && isAssoc($value)){
                    $this->row[0] = $value;
                }
                else {
                    $this->$param = $value;
                }
            }
        }
    }

    public function get($attribute){
        if(substr($attribute, 0, 1) == '@'){
            $attribute = substr($attribute, 1);
        }

        //ToDo: manejar bien cuando hay multiples rows, o que esta funcion solo funcione con resultados con una row
        if(isset($this->row) && isset($this->row['@'.$attribute])){
            return $this->row['@'.$attribute];
        }
        else {
            return null;
        }
    }

    public function getFirst($attribute){
        if(substr($attribute, 0, 1) == '@'){
            $attribute = substr($attribute, 1);
        }

        //ToDo: manejar bien cuando hay multiples rows, o que esta funcion solo funcione con resultados con una row
        if(isset($this->row[0]) && isset($this->row[0]['@'.$attribute])){
            return $this->row[0]['@'.$attribute];
        }
        else {
            return null;
        }
    }

    public function getF($attribute){
        return $this->getFirst($attribute);
    }
}

function getParamsFlat($params = []){
    if(!is_array($params) || empty($params)){
            return "";
    }

    $flat = '';
    $pieces = [];
    foreach($params as $name => $value){
        if(is_array($value)){
            $pieces[] = getParamsFlat($value);
        }
        else {
            $pieces[] = $name."=".$value;
        }
    }
    return implode("&", $pieces);
}

function pISRequest($params = []){
    $session_user = User::find(Auth::id());
    if(!isset($params['Usuario']) || trim($params['Usuario']) == ''){
            $params['Usuario'] = (isset($session_user->usuario_intelisis) && trim($session_user->usuario_intelisis) != "") ? $session_user->usuario_intelisis : config('intelisis.IS_USUARIO');
    }
    if(!isset($params['Referencia']) || trim($params['Referencia']) == ''){
            return new pISResultado(['success' => 0, 'msg' => "No se definio la Referencia"]);
    }

    if(!isset($params['Contrasena']) || trim($params['Contrasena']) == ''){
            $params['Contrasena'] = (isset($session_user->password_intelisis) && trim($session_user->password_intelisis) != "") ? $session_user->password_intelisis : config('intelisis.IS_CONTRASENA');
    }
    if(!isset($params['Empresa']) || trim($params['Empresa']) == ''){
            $params['Empresa'] = config('intelisis.IS_EMPRESA');
    }

    if(!isset($params['RequestURI']) || trim($params['RequestURI']) == ''){
        $requestURI = 'spIntelisisServiceSolicitud';
    }
    else {
        $requestURI = $params['RequestURI'];
        unset($params['RequestURI']);
    }

    if(config('intelisis.ForceCache')){
            $params['forzar'] = time();
    }

    //Buscar solicitud en cache, si existe y no esta expirada, regresarla y no ir al webservice
    if(!isset($params['SubReferencia']) || $params['SubReferencia'] == 'CONSULTA'){
        $cache = App\pisCache::where([
                        ['usuario_intelisis', $params['Usuario']],
                        ['referencia', $params['Referencia']],
                        ['params', getParamsFlat($params)],
                        ['created_at', '>', date('Y-m-d H:m:s', time()-(config('intelisis.RequestDaysTTL')*24*60*60))],
                        ])->first();

        if(!empty($cache) && $cache != null){
                $Resultado = json_decode($cache->resultado);
                if($cache->created_at > date('Y-m-d H:m:s', time()-(5*60) )) {
                        if($Resultado != NULL){
                                if(isset($Resultado->{'@Ok'}) && $Resultado->{'@Ok'} == 0) {
                                    return new pISResultado(array_merge(['success' => 1, 'msg' => ''], json_decode(json_encode($Resultado), true)));
                                }
                        }
                        else {
                            return new pISResultado(array_merge(['success' => 0, 'msg' => 'Resultado de cache nulo'], json_decode(json_encode($Resultado), true)));
                        }
                }
        }
    }

    $client = new \GuzzleHttp\Client();

    $last_memory_size = ini_get('memory_limit');
    if($params['Referencia'] == 'Intelisis.pIS.SocioLista'){
        ini_set('memory_limit','2048M');
    }
    $host = "http://".config('intelisis.IS_URL').':'.config("intelisis.IS_PORT");
    try{
            $result = $client->request('POST', $host.'/'.$requestURI, [
                            'form_params' => $params
            ]);
    }
    catch(\Exception $e){
        return new pISResultado(['success' => 0, 'msg' => $e->getMessage()]);
    }
    //ini_set('memory_limit',$last_memory_size);

    if($result->getHeader('Content-type')[0] == 'text/json'){
        $RespuestaIS = json_decode($result->getBody());
    }
    elseif ($result->getHeader('Content-type')[0] == 'application/octet-stream') {
        return new pISResultado(['success' => 1, 'filestream' => $result->getBody(), 'disposition' => $result->getHeader('Content-Disposition')[0]]);
    }

    if(!isset($RespuestaIS->success) || !$RespuestaIS->success){
            if(!isset($RespuestaIS->msg) || trim($RespuestaIS->msg) == "Ocurrio un error al obtener los datos requeridos"){
                    return new pISResultado(['success' => 0, 'msg' => ""]);
            }
            else {
                    return new pISResultado(['success' => 0, 'msg' => $RespuestaIS->msg]);
            }
    }

    if(!isset($RespuestaIS->ResultadoIS)){
            return new pISResultado(['success' => 0, 'msg' => "No se recibio un objeto Resultado correcto"]);
    }

    $Resultado = $RespuestaIS->ResultadoIS->Intelisis->Resultado;

    if($Resultado->{'@Ok'} != NULL){
            if(trim($Resultado->{'@OkRef'}) != ''){
                    $msg = $Resultado->{'@Mensaje'}.'. Ref: '.$Resultado->{'@OkRef'};
            }
            else {
                    $msg = $Resultado->{'@Mensaje'};
            }
            return new pISResultado(['success' => 0, 'msg' => $msg]);
    }

    if(isset($Resultado->{'@Ok'}) && $Resultado->{'@Ok'} == 0){
        if(!isset($params['SubReferencia']) || $params['SubReferencia'] == 'CONSULTA'){
            if(isset($params['force'])) unset($params['force']);

            App\pisCache::where([
                            'usuario_intelisis' => $params['Usuario'],
                            'referencia' => $params['Referencia'],
                            'params' => getParamsFlat($params),
                            /*'resultado' => json_encode($Resultado),*/ //Creo que no necesitamos comparar resultado aqui. Lo que importa son los params
            ])->delete();

            $new_cache = App\pisCache::create([
                            'usuario_intelisis' => $params['Usuario'],
                            'referencia' => $params['Referencia'],
                            'params' => getParamsFlat($params),
                            'resultado' => json_encode($Resultado),
            ]);
        }
        
        $msg = '';
        if(isset($RespuestaIS->ResultadoIS->Intelisis->Resultado->{'@Msg'}) && trim($RespuestaIS->ResultadoIS->Intelisis->Resultado->{'@Msg'}) != ''){
            $msg = $RespuestaIS->ResultadoIS->Intelisis->Resultado->{'@Msg'};
        }
        return new pISResultado(array_merge(['success' => 1, 'msg' => $msg], json_decode(json_encode($Resultado), true)));
    }
    else {
            return new pISResultado(array_merge(['success' => 0, 'msg' => 'Error Generico'], json_decode(json_encode($Resultado), true)));
    }
}